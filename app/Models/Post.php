<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use Spatie\Tags\HasTags;

class Post extends Model
{
    use HasFactory;
    use HasTags;
    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getShortDescriptionAttribute()
    {
        return Str::words($this->content, 40, '...');
    }


    public  function getStatus(){

        $status = $this->status;
        switch ($status) {
            case "0":
                echo '<span class="badge badge-outline-primary">DRAFT</span>';
                break;
            case "1":
                echo '<span class="badge badge-success">PUBLISHED</span>';
                break;
            case "2":
                echo '<span class="badge badge-success">PUBLISHED</span>';
                break;
        }


    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'post_categories');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }


    public function getCategoryAttribute()
    {
        return $this->category()->pluck('id')->toArray();
    }




    public function getTagListAttribute()
    {
        return $this->tags()->pluck('name');
    }



    public  function getLanguage(){

        $locale = $this->locale;
        switch ($locale) {
            case "en":
                echo '<span class="badge badge-outline-primary">ENGLISH</span>';
                break;
            case "ml":
                echo '<span class="badge badge-outline-primary">MALAYALAYAM</span>';
                break;
        }
    }
}
