<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{

    protected $fillable = ['title','slug','status'];
    use NodeTrait;
    use HasSlug;
    use HasFactory;

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCategoryAttribute()
    {
        return $this->category()->pluck('id')->toArray();
    }

    public function posts()
    {
        return $this->belongsToMany(Business::Class, 'post_categories');
    }

    public function business()
    {
        return $this->belongsToMany(Business::Class,'business_categories');
    }



}
