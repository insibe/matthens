<?php

namespace App\Models;
use Carbon\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{

    protected $fillable = ['user_id','cr_no','cr_en','cr_ar','cr_type','cr_status','date_registered','date_expired','cr_status','country','status' ];
    use HasFactory;

    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('cr_en')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'business_categories')->withTimestamps();
    }




//    public function getUpdatedAtAttribute($value) {
//        return \Carbon\Carbon::parse($value)->format('d M Y');
//    }





//    public function getDateRegisteredAttribute($value) {
//        return Carbon::createFromDate($value)->diff(Carbon::now())->format('%y years, %m months and %d days');
//    }
}
