<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Imports\BusinessImport;
use App\Models\Category;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses =  Business::paginate(15);
        $data = [
            'page_title' => 'Manage Businesses'
        ];

        return view('dashboard.businesses.index',compact('businesses'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $business = Business::where('id', $id)
            ->orWhere('slug', $id)
            ->firstOrFail();

        return view('business-single',compact('business') );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Business::where('id', $id)->firstOrFail();
        $categoriesPrimary =   Category::whereIsRoot()->pluck('title','id');

        $data = [
            'business' => $business,
            'formMethod' => 'PUT',
            'url' => 'businesses/'.$id,
            'page_title' => ' Edit '.$business->title
        ];

        return view('business-edit',compact('categoriesPrimary'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Business $business)
    {
        //
    }


    public function import(Request $request)
    {

        Excel::import(new BusinessImport,$request->import_file);
        return redirect('/')->with('success', 'All good!');
    }


    public function search(Request $request){
        // Get the search value from the request
        $search = $request->input('search');

        // Search in the title and body columns from the posts table
        $businesses = Business::query()
            ->where('cr_en', 'LIKE', "%{$search}%")
            ->get();


        // Return the search view with the resluts compacted
        return view('search', compact('businesses'));
    }

}
