<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use App\Models\Feed;
use App\Models\Tag;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
// import the Intervention Image Manager Class
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic as Image;
use GoogleTranslate;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Config;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller

{

    protected $url = 'https://www.gulf-insider.com/wp-json/wp/v2/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts =   Post::orderBy('created_at', 'desc')->paginate(10);
        $data = [
            'page_title' => 'Manage Posts'
        ];

        return view('dashboard.posts.index',compact('posts'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $categories =   Category::pluck('title','id');
        $userList = User::pluck('name','id');
        $feedSource = Feed::pluck('title','id')->all();
        $tagsList  = Tag::pluck('name', 'name');


        $data = [
            'post' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/posts',
            'page_title' => 'Add a New Post'
        ];

        return view('dashboard.posts.edit', compact('categories','tagsList','userList','feedSource'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $post = new Post();
        $post->source_id                = '0-1-EN';
        $post->user_id                  = '1';
        $post->title                    = $request->get('title');
        $post->excerpt                  = $request->get('excerpt');
        $post->content                  = $request->get('content');
        $post->post_url                 = $request->get('post_url');
        $post->locale                   = $request->get('locale');
        $post->feed_id                  = $request->get('feed_id');
        $post->published_at             = Carbon::now()->toDateTimeString();
        $post->sticky                   = '0';
        $post->save();




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('id', $id)->firstOrFail();
        $categories =   Category::pluck('title','id');
        $userList = User::pluck('name','id');
        $feedSource = Feed::pluck('title','id')->all();

        $tagsList  = Tag::pluck('name', 'name');


        $data = [
            'post' => $post,
            'formMethod' => 'PUT',
            'url' => 'dashboard/posts/'.$id,
            'page_title' => ' Edit #'.$post->id
        ];

        return view('dashboard.posts.edit', compact('categories','tagsList','userList','feedSource'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $destination    = '/images/posts/'; // image file upload path
        $saveFlag       = 0;


        $tags = $request->get('tag_list');

        try {

            $post = Post::findOrFail($id);

            if ($request->hasFile('featured_image')) {
                $file       = $request->file('featured_image');
                $extension  = $file->getClientOriginalExtension(); // getting image extension
                $fileName   = 'gulfthis-glance'.time().'.'.$extension; // renameing image
                Storage::disk('public')->putFileAs($destination,$file, $fileName);
                $featured_image   = $destination.$fileName;//file name for saving to db

                $request_image = $request->file('featured_image');

                $image = Image::make($request_image);
                // keep ratio height and width
                $thumbnailpath = public_path('storage/images/posts/thumbnail/'.$fileName);
                $image->fit(800, 600, function ($constraint) {
                    $constraint->upsize();
                });
                $image->text('The quick brown fox jumps over the lazy dog.', 120, 100);
                $image->save($thumbnailpath);

              $thumbPath =  $destination.'thumbnail/'.$fileName ;

            }else{
                $featured_image = $post->featured_image;
            }

            $post->title                         = $request->get('title');
            $post->slug                          = $request->get('title');
            $post->excerpt                       = $request->get('excerpt');
            $post->content                       = $request->get('content');
            $post->post_url                      = $request->get('post_url');
            $post->featured_image                = $thumbPath ?? '';
            $post->locale                        = $request->get('locale');
            $post->status                        = $request->get('status');
            $post->feed_id                       = $request->get('feed_id');
            $post->save();

            $post->syncTags($tags);
            $post->categories()->sync($request->input('categories'));

            Alert::success('Success', 'Post Updated Successfully');
            return redirect('dashboard/posts/'.$post->id.'/edit')->with('success', 'Post Updated Successfully!');

        } catch (\Exception $e) {
            return $e;
        }









    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }


    public function demo()
    {



        $url =  'https://starofmysore.com/wp-json/wp/v2/posts/';
//
        $response = file_get_contents($url);
//
////        $response = '{"1":"a","2":"b","3":"c","4":"d","5":"e"}';
        $posts = json_decode($response, true);
//
        foreach ($posts as $key => $value) {
//
            $post = new Post();
            $post->post_id                  = $value["id"];
            $post->user_id                  = '1';
            $post->title                    = GoogleTranslate::justTranslate( $value["title"]["rendered"]);
            $post->slug                     = $value["title"]["rendered"];
            $post->excerpt                  = GoogleTranslate::justTranslate( $value["title"]["rendered"]);
            $post->content                  = GoogleTranslate::justTranslate( $value["content"]["rendered"]);
            $post->featured_image           = '';
            $post->featured_image_caption   = $value["title"]["rendered"];
            $post->published_at             = time().now();
            $post->sticky                   = '0';
            $post->save();
//
//


//
        }
//

//        $data = [
//            'title' => $newsData[0]['title']['rendered'],
//            'img' => $newsData[0]['uagb_featured_image_src']['full'],
//            'link' => $newsData[0]['link'],
//        ];





//        foreach ($data->get_items(0,$max) as $item)
//        {
//            $post = new Post();
//            $post->post_id                  = '11';
//            $post->user_id                  = '1';
//            $post->title                    = $item->get_title();
//            $post->excerpt                  = '';
//            $post->content                  = $item->get_description();
//            $post->featured_image           = '';
//            $post->featured_image_caption   = $item->get_title();
//            $post->published_at             = $item->get_title();
//            $post->sticky                   = '0';
//            $post->save();
//
//        }
//
//
//        $img = Image::make($data['img'][0]);
//
//        $img->text($data['title'], 120, 100, function($font) {
//            $font->size(100);
//            $font->color('#e1e1e1');
//            $font->align('center');
//            $font->valign('bottom');
//            $font->angle(90);
//        });
//        $img->save(public_path('images/'.$data['title'].'.jpg'));

    }



}
