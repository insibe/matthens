<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedRequest;
use App\Models\Feed;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use GoogleTranslate;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds =  Feed::paginate(15);
        $data = [
            'page_title' => 'Manage Feeds'
        ];

        return view('dashboard.feeds.index',compact('feeds'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'feed' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/feeds',
            'page_title' => 'Add a New Feed'
        ];

        return view('dashboard.feeds.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedRequest $request)
    {
        $destination    = 'images/categories/'; // image file upload path
        $saveFlag       = 0;
//        $iconfileName     = "";
//        $coverfileName     = "";

        try {
            //upload image
//            if ($request->hasFile('icon')) {
//                $file       = $request->file('icon');
//                $extension  = $file->getClientOriginalExtension(); // getting image extension
//                $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'icon'.'_'.time().'.'.$extension; // renameing image
//                Storage::putFileAs($destination,$file, $fileName);
//                $iconfileName   = $destination.$fileName;//file name for saving to db
//
//            }
//            if ($request->hasFile('cover')) {
//                $file       = $request->file('cover');
//                $extension  = $file->getClientOriginalExtension(); // getting image extension
//                $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'cover'.'_'.time().'.'.$extension; // renameing image
//                Storage::putFileAs($destination,$file, $fileName);
//                $coverfileName   = $destination.$fileName;//file name for saving to db
//
//            }




            $feed = new Feed();
            $feed->title                 = $request->get('title');
            $feed->description           = $request->get('description');
            $feed->source_url            = $request->get('source_url');
            $feed->source_lang           = $request->get('source_lang');
            $feed->target_lang           = $request->get('target_lang');
            $feed->logo                  = $request->get('logo');
            $feed->type                  = $request->get('type');
            $feed->interval              = $request->get('interval');
            $feed->website               = $request->get('website');
            $feed->status                = $request->get('status');
            $feed->save();


            return redirect('dashboard/feeds/'.$feed->id.'/edit')->with('success', 'Category Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $feed = Feed::where('id', $id)->firstOrFail();


        $data = [
            'feed' => $feed,
            'formMethod' => 'PUT',
            'url' => 'dashboard/feeds/'.$id,
            'page_title' => ' Edit '.$feed->title
        ];

        return view('dashboard.feeds.edit' ,$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $feed = Feed::findOrFail($id);

            $feed->title                 = $request->get('title');
            $feed->description           = $request->get('description');
            $feed->source_url            = $request->get('source_url');
            $feed->source_lang           = $request->get('source_lang');
            $feed->target_lang           = $request->get('target_lang');
            $feed->logo                  = $request->get('logo');
            $feed->type                  = $request->get('type');
            $feed->interval              = $request->get('interval');
            $feed->website               = $request->get('website');
            $feed->status                = $request->get('status');

            $feed->save();
            Alert::success('Success', 'Feed Update  Successfully');
            return redirect('dashboard/feeds/'.$feed->id.'/edit')->with('success', 'Feed Update  Successfully!');

        }
        catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feed = Feed::find($id);
        $feed->delete();
        return redirect('dashboard/feeds/')->with('success', 'Feed Deleted Successfully!');
    }

    public function fetch($id)
    {
        $feed = Feed::where('id', $id)->firstOrFail();

        switch($feed->type) {
            case('rss'):

            break;

            case('wapi'):





                $source_url = $feed->source_url;
                $response = file_get_contents($source_url);
                $posts = json_decode($response, true);
                foreach ($posts as $key => $value) {

                    $sourceID = $feed->id.'-'.$value["id"].'-'.strtoupper($feed->target_lang);

                    $found = Post::where('source_id', $sourceID )->first();

                    if (! $found) {
                        $post = new Post();
                        $post->source_id                = $sourceID;
                        $post->user_id                  = '1';
                        $post->title                    = GoogleTranslate::justTranslate( $value["title"]["rendered"],$feed->target_lang);
                        $post->excerpt                  = GoogleTranslate::justTranslate( $value["title"]["rendered"],$feed->target_lang);
                        $post->content                  = strip_tags(GoogleTranslate::justTranslate( $value["content"]["rendered"],$feed->target_lang));
                        $post->post_url                 = $value["link"];
                        $post->locale                   = $feed->target_lang;
                        $post->feed_id                   = $feed->id;
                        $post->published_at             = Carbon::now()->toDateTimeString();
                        $post->sticky                   = '0';
                        $post->save();
                    }



                }
//

            break;

            default:
              echo 'Something went wrong.';

        }


        $feed->last_fetch    = Carbon::now()->toDateTimeString();


        $feed->save();

        Alert::success('Success Title', 'Success Message');

        return redirect('dashboard/feeds/')->with('success', 'Feed Deleted Successfully!');
    }

}
