<?php

namespace App\Http\Controllers;

use App\Imports\BusinessImport;
use App\Imports\CategoryImport;
use App\Models\Business;
use App\Models\Category;
use Illuminate\Http\Request;
use Kalnoy\Nestedset\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories =  Category::paginate(15);

        $data = [
            'page_title' => 'Manage Categories'
        ];

        return view('dashboard.category.index',compact('categories'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesList =    $this->getCategoryOptions();

        $data = [
            'category' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/categories',
            'page_title' => 'Add a New Category'
        ];

        return view('dashboard.category.edit',compact('categoriesList'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination    = 'images/categories/'; // image file upload path
        $saveFlag       = 0;
//        $iconfileName     = "";
//        $coverfileName     = "";




        try {


            if ($request->hasFile('cover')) {
                $file       = $request->file('cover');
                $extension  = $file->getClientOriginalExtension(); // getting image extension
                $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'cover'.'_'.time().'.'.$extension; // renameing image
                Storage::putFileAs($destination,$file, $fileName);
                $coverfileName   = $destination.$fileName;//file name for saving to db

            }
            if ($request->hasFile('icon')) {
                $file       = $request->file('icon');
                $extension  = $file->getClientOriginalExtension(); // getting image extension
                $fileName   = str_replace(" ","-",strtolower($request->get('title'))).'_'.'icon'.'_'.time().'.'.$extension; // renameing image
                Storage::putFileAs($destination,$file, $fileName);
                $iconfileName   = $destination.$fileName;//file name for saving to db

            }

            $category = new Category();
            $category->title           = $request->get('title');
            $category->locale          = $request->get('locale');
            $category->description     = $request->get('description');
            $category->parent_id       = $request->get('parent_id');
            $category->type            = $request->get('type');
            $category->cover           = $coverfileName ?? null;
            $category->icon            = $iconfileName ?? null;
            $category->status          = $request->get('status') ?? 0;
            $category->save();
            if($request->parent_id && $request->parent_id !== 'none') {
                //  Here we define the parent for new created category
                $node = Category::find($request->parent_id);

                $node->appendNode($category);
            }

            return redirect('dashboard/categories/'.$category->id.'/edit')->with('success', 'Category Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('id', $id)
            ->orWhere('slug', $id)
            ->firstOrFail();

        $businesses = $category->business()->whereIn('cr_status', array('ACTIVE'))->orderBy('date_registered','desc')->paginate(10);

        return view('category-single',compact('category','businesses') );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $categoriesList = $this->getCategoryOptions($category);

        $data = [
            'category' => $category,
            'formMethod' => 'PUT',
            'url' => 'category/'.$id,
            'page_title' => ' Edit '.$category->title
        ];

        return view('dashboard.category.edit', compact('categoriesList'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        try {
            $category = Category::findOrFail($id);

            $category->title           = $request->get('title');
            $category->description     = $request->get('description');
            $category->parent_id       = $request->get('parent_id');

            $category->save();
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    public function import(Request $request)
    {

        Excel::import(new CategoryImport,$request->import_file);
        return redirect('/')->with('success', 'All good!');
    }


    protected function makeOptions(Collection $items)
    {
        $options = [' ' => 'None' ];
        foreach ($items as $item)
        {
            $options[$item->getKey()] = str_repeat('-', $item->depth + 2).''.$item->title;
        }
        return $options;
    }

    /**
     * @param Category $except
     *
     * @return CategoriesController
     */
    protected function getCategoryOptions($except = null)
    {
//        /** @var \Kalnoy\Nestedset\QueryBuilder $query */
        $query = Category::select('id', 'title')->withDepth();
        if ($except)
        {
            $query->whereNotDescendantOf($except)->where('id', '<>', $except->id);
        }
        return $this->makeOptions($query->get());
    }


    public function fixNodesTree()
    {
        Category::fixTree();
        return redirect()->back();
    }
}
