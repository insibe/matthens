<?php

namespace App\Imports;
use App\Models\Business;
use App\Models\BusinessCategory;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;



class BusinessImport implements ToModel, WithBatchInserts
{
    public function model(array $row)
    {
        return new Business([

            'user_id' => auth()->id(),
            'cr_no' => $row['no'],
            'cr_en' => $row['en'],
            'cr_ar' => $row['ar'],
            'cr_type' => $row['type'],
            'date_registered' =>  Carbon::parse($row['registered'])->format('Y-m-d'),
            'date_expired' =>  Carbon::parse($row['expired'])->format('Y-m-d'),
            'cr_status' => $row['cr'],
            'country' => 'Bahrain',
            'status' => 'active',

        ]);


    }

    public function batchSize(): int
    {
        return 1000;
    }
}


//class BusinessImport implements ToModel,  WithBatchInserts
//{
//    public function onRow(Row $row){
//        $rowIndex = $row->getIndex();
//       $row      = $row->toArray();
//
//
//        $business = Business::firstOrCreate([
//            'user_id' => auth()->id(),
//            'cr_no' => $row['no'],
//            'cr_en' => $row['en'],
//            'cr_ar' => $row['ar'],
//            'cr_type' => $row['type'],
//            'date_registered' =>  Carbon::parse($row['registered'])->format('Y-m-d'),
//            'date_expired' =>  Carbon::parse($row['expired'])->format('Y-m-d'),
//            'cr_status' => $row['cr'],
//            'country' => 'Bahrain',
//            'status' => 'active',
//       ]);
//
//        $id =  $business->id;
//
//        $BusinessCategory = BusinessCategory::firstOrCreate([
//            'business_id' => $id,
//            'category_id' => '15',
//
//      ]);
//
//    }
//
//    public function batchSize(): int
//    {
//        return 1000;
//    }
//}
//


//class BusinessImport implements ToModel, WithHeadingRow,WithBatchInserts
//{
////    public function onRow(Row $row)
////    {
////        $rowIndex = $row->getIndex();
////        $row      = $row->toArray();
////
////
////
////        $business = Business::firstOrCreate([
////            'user_id' => auth()->id(),
////            'cr_no' => $row['no'],
////            'cr_en' => $row['en'],
////            'cr_ar' => $row['ar'],
////            'cr_type' => $row['type'],
////            'date_registered' =>  Carbon::parse($row['registered'])->format('Y-m-d'),
////            'date_expired' =>  Carbon::parse($row['expired'])->format('Y-m-d'),
////            'cr_status' => $row['cr'],
////            'country' => 'Bahrain',
////            'status' => 'active',
////        ]);
////
////
////
////        $id =  $business->id;
////
////        $BusinessCategory = BusinessCategory::firstOrCreate([
////
////            'business_id' => $id,
////            'category_id' => '15',
////
////        ]);
////
////
////    }
//}
