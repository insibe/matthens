<?php

use  Carbon\Carbon;
use GuzzleHttp\Client;

use App\Models\Business;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('ml.gulfthis.com')->group(function () {
    Route::get('/', function () {
        return 'App 1'; // Home page for the APP 1
    });
});


Route::get('/malayalam', function () {
    $categoriesList =  Category::all();
    $posts =   Post::where('status','2')->where('locale','ml')->orderBy('id', 'desc')->paginate(10);

    $businesses =  Business::all()
        ->random(21);
    return view('welcome',compact('categoriesList','posts') );
});


Route::get('/', function () {

    $categoriesList =  Category::all();

    $posts =   Post::where('status','2')->where('locale','en')->orderBy('id', 'desc')->paginate(10);

    $businesses =   Business::all()
        ->random(21);

    return view('welcome',compact('businesses','categoriesList','posts') );
});


Route::get('/businesses/{businesses-name}', function ($slug) {
    $business = Business::where('id', $slug)
        ->orWhere('slug', $slug)
        ->firstOrFail();
    return view('business-single',compact('business') );

});






Route::get('/finance/gold-rate-in-bahrain', function () {



    return view('gold-rate');
});


Route::get('/search/', [\App\Http\Controllers\BusinessController::class, 'search'])->name('search');

//Route::get('/dashboard', function () {
//    return view('dashboard.index' );
//});


Route::get('/duplicate', function () {
    $business = Business::all();
    $businessUnique = $business->unique(['cr_no']);
    $businessDuplicates = $business->diff($businessUnique);
    echo "<pre>";
    print_r($businessDuplicates->count());
});



//Route::get('/fixNodesTree', [\App\Http\Controllers\CategoryController::class, 'fixNodesTree'])->name('fixNodesTree');

//Route::post('/import', [\App\Http\Controllers\CategoryController::class, 'import'])->name('import');

Route::post('/import', [\App\Http\Controllers\BusinessController::class, 'import'])->name('import');






//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
Route::get('/api/getShorts', function () {

    $posts =   Post::where('status','2')->where('locale','en')->orderBy('id', 'desc')->paginate(10);
    return $posts;
});

Route::get('/home', function () {
    return view('home' );
});

Route::get('/guides', function () {
    return view('about-us' );
});



Route::get('/automotive', function () {
    return view('automotive' );
});

Route::get('company/about-us', function () {
    return view('about-us' );
});

Route::get('company/investor-relations', function () {
    return view('investor-relations' );
});

Route::get('company/editorial', function () {
    return view('editorial' );
});

Route::get('company/media-kit', function () {
    return view('media-kit' );
});

Route::get('company/careers', function () {
    return view('careers' );
});

Route::get('company/contact-us', function () {
    return view('contact-us' );
});

Route::get('legal/disclaimer', function () {
    return view('about-us' );
});

Route::get('terms-of-use', function () {
    return view('terms-of-use' );
});

Route::get('contact-us', function () {
    return view('about-us' );
});

Route::get('public-holidays-in-bahrain', function () {
    return view('public-holidays' );
});
Route::get('bahrain-indemnity-calculator', function () {
    return view('indemnity-calculator' );
});

Route::get('update-gold-price', function () {
    $response = Http::get( 'https://data-asg.goldprice.org/dbXRates/INR' );
// Array of data from the JSON response
    $data = $response->json();

    return $data['items'][0]['xauPrice']/28.35;
});







Route::get('covid-19-updates-bahrain', function () {
    return view('indemnity-calculator' );
});

Route::get('list-companies-in-bahrain', function () {

    $businesses =  Business::paginate('20');

    return view('business',compact('businesses') );


});

Route::get('/news', function () {

    $posts =  Post::orderBy('created_at', 'DESC')->paginate('100');
    return view('news',compact('posts') );
});

Route::resource('businesses', \App\Http\Controllers\BusinessController::class);

Route::get('update-feed', [\App\Http\Controllers\PostController::class, 'demo']);




Route::group(['middleware' => 'auth'], function () {
    // Admin Dashboard
    Route::get('/dashboard', function () {
        return view('dashboard.index' );
    });
    Route::resource('dashboard/gold-rate', \App\Http\Controllers\GoldRateController::class);
    Route::resource('dashboard/category', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/posts', \App\Http\Controllers\PostController::class);
//    Route::resource('dashboard/businesses', \App\Http\Controllers\BusinessController::class);
    Route::resource('dashboard/feeds', \App\Http\Controllers\FeedController::class);
    Route::get('dashboard/feeds/{id}/fetch', [\App\Http\Controllers\FeedController::class, 'fetch']);
//    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/users', \App\Http\Controllers\UserController::class);


    Route::get('dashboard/settings/system-log', function () {
        $systemLogs = \Venturecraft\Revisionable\Revision::paginate('30');
        return view('dashboard.settings.system-log',compact('systemLogs'));
    });

});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('/migrate', function(){
    Artisan::call('migrate');
    return '<h1>MIGRATE</h1>';
});



