<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;



    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->colorName;
        $slug = Str::slug($title, '-');

        return [
            'title' =>  $title ,
            'slug' => $slug,
            'description' => $this->faker->paragraph(3),
            'cover' => $this->faker->imageUrl(900, 300),
            'status' => $this->faker->randomElement(['expired', 'active', 'disabled']),

        ];
    }
}
