<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\BusinessCategory;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'business_id' =>   Business::inRandomOrder()->first()->id,
            'category_id' =>  Category::inRandomOrder()->first()->id,
        ];
    }
}
