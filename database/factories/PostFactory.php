<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence($nbWords = 6, $variableNbWords = true);
        $slug = Str::slug($title, '-');

        return [
            'title' =>  $title ,
            'slug' => $slug,
            'source_id' => $this->faker->unique()->numberBetween(1,50),
            'user_id' => User::all()->random()->id,
            'excerpt' => $this->faker->paragraph(3),
            'content' => $this->faker->paragraph(10),
            'published_at' => $this->faker->dateTime($max = 'now', $timezone = null),
            'sticky' => $this->faker->randomElement(['0', '1']),
            'featured_image' =>  $this->faker->imageUrl(1280, 800, 'cats', true, 'Faker', true), // 'http://lorempixel.com/gray/800/400/cats/Faker/' Monochrome image,
            'status' => $this->faker->randomElement(['draft', 'scheduled', 'published']),

        ];
    }
}
