<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {



        $BusinessCategory = $this->BusinessCategory();
        $title = $this->faker->company;
        $slug = Str::slug($title, '-');


        return [
            'user_id' =>  User::factory(),
            'cr_no' =>  $this->faker->numberBetween(1,20000).''.$this->faker->numberBetween(1,6),
            'cr_en' =>  $title ,
            'cr_ar' =>  'شركة انسايب للتكنولوجيا ذ.م.م' ,
            'slug' => $slug,
            'date_registered' => $this->faker->dateTimeBetween('-30 years', 'now'),
            'date_expired' => $this->faker->dateTimeBetween('-30 years', 'now'),
            'country' => $this->faker->country,
            'cr_type' => $this->faker->randomElement(['Branch of a Foreign Company', 'Commandite by Shares Company', 'Investment Limited Partnerships','Bahrain Shareholding Company (Public)', 'Bahrain Shareholding Company (Closed)', 'Single Commodite Company','Single Person Company (SPC)', 'With Limited Liability (WLL)', 'Individual Establishment', 'Partnership Company', 'Protected Cell Company']),
            'cr_status' => $this->faker->randomElement(['ACTIVE ', 'DELETED BY LAW', 'ACTIVE WITHOUT LICENCE']),
            'status' => $this->faker->randomElement(['expired', 'active', 'disabled']),
        ];
    }


    public function BusinessCategory()
    {
        return $this->faker->randomElement([
            Business::class,
            Category::class,
        ]);
    }
}
