
@extends('layouts.app')


@section('content')
    <section class="heroBg" style="background: url('{{ asset('images/buildings.svg') }}') no-repeat bottom center  #0051d4  " >
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h2 class="headline">Bahrain's No.1 Local Search Engine</h2>
                    <p class="subline">Company reviews. Salaries. Interviews. Jobs.</p>
                    <div class="col-md-6 col-md-offset-3">
                        <form  action="{{ route('search') }}" method="GET">

                            <div class="input-group input-group-lg">
                                <input class="form-control form-control-lg" name="search" placeholder="Search for anything, anywhere in Bahrain" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Go</button> </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>



                </div>

            {{--                <div class="row mt-3">--}}
            {{--                    <div class="col-sm-12">--}}
            {{--                        <a href="#">Web Design Company In Bahrain</a> |--}}
            {{--                        <a href="#">Attractions</a> |--}}
            {{--                        <a href="#">Stories</a> |--}}
            {{--                        <a href="#">Guides</a>--}}

            {{--                    </div>--}}
            {{--                </div>--}}
            <!-- row -->
            </div>

        </div>


    </section>
<div class="container my-md-4 ">
  <div class="row">
      <div class="col-lg-12">
          <h3>List of companies in Bahrain</h3>
          <p>{{ \App\Models\Business::whereIn('cr_status', array('ACTIVE'))->count()  }} unique companies found</p>
      </div>
      <div class="col-lg-3">

      </div>
      <div class="col-lg-9">
          <div class="row">
              <div class="col-12">
                  <div class="col-12">
                      <div class="business-list">
                          <ul>
                              @if(count($businesses ) > 0)
                                  @foreach($businesses as $business)

                                          <div class="card">
                                              <div class="card-body">
                                                  <div class="row align-items-center">
                                                      <div class="col-auto">

                                                          <!-- Image -->
                                                          <a href="{{ url('/businesses/'.$business->slug ) }}" class="avatar avatar-lg">
                                                              <img src="https://static.ambitionbox.com/static/icons/company-placeholder.svg" alt="{{ $business->cr_en }}" class="avatar-img rounded">
                                                          </a>

                                                      </div>
                                                      <div class="col ms-n2">

                                                          <!-- Heading -->
                                                          <h4 class="mb-1">
                                                              <a href="{{ url('/businesses/'.$business->slug ) }}">{{ $business->cr_en }}</a>
                                                          </h4>

                                                          <!-- Text -->
                                                          <p class="small text-muted mb-1">
                                                              Launchday is a SaaS website builder with a focus on quality, easy to build product sites.
                                                          </p>

                                                          <!-- Time -->
                                                          <small class="text-muted">
                                                              <i class="fe fe-clock"></i> Updated 2hr ago
                                                          </small>

                                                      </div>
                                                      <div class="col-auto">

                                                          <!-- Avatar group -->
                                                          <div class="avatar-group d-none d-md-inline-flex">
                                                              <a href="profile-posts.html" class="avatar avatar-xs">
                                                                  <img src="../assets/img/avatars/profiles/avatar-2.jpg" class="avatar-img rounded-circle" alt="...">
                                                              </a>
                                                              <a href="profile-posts.html" class="avatar avatar-xs">
                                                                  <img src="../assets/img/avatars/profiles/avatar-3.jpg" class="avatar-img rounded-circle" alt="...">
                                                              </a>
                                                              <a href="profile-posts.html" class="avatar avatar-xs">
                                                                  <img src="../assets/img/avatars/profiles/avatar-4.jpg" class="avatar-img rounded-circle" alt="...">
                                                              </a>
                                                              <a href="profile-posts.html" class="avatar avatar-xs">
                                                                  <img src="../assets/img/avatars/profiles/avatar-5.jpg" class="avatar-img rounded-circle" alt="...">
                                                              </a>
                                                          </div>

                                                      </div>
                                                      <div class="col-auto">

                                                          <!-- Dropdown -->
                                                          <div class="dropdown">
                                                              <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                  <i class="fe fe-more-vertical"></i>
                                                              </a>
                                                              <div class="dropdown-menu dropdown-menu-end">
                                                                  <a href="#!" class="dropdown-item">
                                                                      Action
                                                                  </a>
                                                                  <a href="#!" class="dropdown-item">
                                                                      Another action
                                                                  </a>
                                                                  <a href="#!" class="dropdown-item">
                                                                      Something else here
                                                                  </a>
                                                              </div>
                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                          </div>

                                  @endforeach
                              @else

                              @endif
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>

  </div>
</div>


@endsection