
@extends('layouts.app')
@section('content')
<div class="container my-md-4 ">
  <div class="row">
      <div class="col-8">
          <div class="card">
            <div class="card-header">
                <h3 class="card-header-title">Results for "v" </h3>
            </div>
              <div class="card-body">
                  <p>We found over  {{ $businesses->count() }} relevant matches for your search term.</p>
                  <p>Bahrain Business Directory in this website lists all business in Bahrain with absolute contact details and industry details. All the listings in this directory of business in Bahrain are free and wont attract any kind of charges from business in Bahrain. All business listed here will be reviewed from time to time. To add your business please click on the green button below.</p>
              </div>



          </div>


          @if($businesses->isNotEmpty())

              @foreach($businesses as $business)
                  <li>
                      <div class="card">
                       <div class="card-header">
                           <a href="{{ url('businesses/'.$business->slug ) }}"><h4 class="card-header-title">{{ ucwords(strtolower($business->cr_en))}}</h4></a>
                       </div>
                          <p></p>
                      </div>
                  </li>
              @endforeach

          @else
              <div class="card">
                 <div class="card-header">
                     <h2 class="card-header-title" >No posts found</h2>
                 </div>
              </div>
          @endif


          <div class="row">
              <div class="col-12">
                  <div class="col-12">
                     <div class="category-list">

                     </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-12">



              </div>
          </div>
      </div>
      <div class="col-4">

      </div>
  </div>
</div>


@endsection