@extends('layouts.app')

@section('content')

    <section class="banner_hero pt-lg-5" style="background-image: url('{{ asset('img/home-page-banner-400.png') }}')">
        <div class="wrapper">
            <div class="container">


                <div class="col-lg-5 col-sm-12 mt-sm-5 mt-xl-5">
                    <div class="search_box">
                        <h3>Let's find your perfect car</h3>
                        <p>Simply tell us your budget, preferred body type & type of vehicle.</p>
                        <form method="post" action="grid-listings-filterscol.html">
                            <div class="form-group">
                                <select class="custom-select select2">
                                    <option selected>Select Your Budget</option>
                                    <option value="1">1,000 - 5,000 BHD</option>
                                    <option value="2">6,000 - 10,000</option>
                                    <option value="3">21,000 - 50,000</option>
                                    <option value="4">50,000 - 100,000</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="custom-select  select2">
                                    <option selected>preferred body type</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>

                            <input class="btn btn-primary btn-lg btn-block" type="submit " value="Search">
                        </form>
                    </div>

                </div>


            </div>

        </div>

    </section>
    <section class="services_hero section--white section--pad">

        <div class="container margin_80_55">
            <div class="section--title">

                <h2>What are you looking for?</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div class="row justify-content-center">

                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/new') }}" class="service_box">

                        <img src="{{ asset('images/find_car_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Find New Car</h3>
                        <p>With exciting offers</p>

                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/recommend-cars') }}" class="service_box">

                        <img src="{{ asset('images/car_recommder_icon.svg') }} " width="65" height="65" alt="">
                        <h3>Recommended a Car</h3>
                        <p>With exciting offers</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/listings/auto-rentals') }}" class="service_box">

                        <img src="{{ asset('images/car_rental_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Auto Rentals</h3>
                        <p>With exciting offers</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/listings/auto-loans') }}" class="service_box">
                        <img src="{{ asset('images/car_loan_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Auto Loans</h3>
                        <p>With exciting offers</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="{{ url('/used-cars') }}" class="service_box">

                        <img src="{{ asset('images/find_car_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Find Used Car</h3>
                        <p>pre-owned cars for sala</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/sell-your-car') }}" class="service_box">

                        <img src="{{ asset('images/sell_car_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Sell Your Car</h3>
                        <p>at the best price</p>

                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/listings/auto-care') }}" class="service_box">

                        <img src="{{ asset('images/car_care_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Auto Care</h3>
                        <p>at the best price</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-xl-3">
                    <a href="{{ url('/listings/auto-accessories') }}" class="service_box">

                        <img src="{{ asset('images/car-accessories_icon.svg') }}" width="65" height="65" alt="">
                        <h3>Auto Accessories</h3>
                        <p>at the best price</p>
                    </a>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->

    </section>
    <section class="featured_hero section--pad">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="section--title">

                        <h2>Recommended Cars For You</h2>
                        <p>Discover a selection of cars with even bigger savings - for a limited time only!</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="featured_item_single">
                        <div class="strip grid">
                            <figure>

                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                            </figure>
                            <div class="wrapper">
                                <h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
                                <p>Rs 5.75 - 9.10 Lakh*</p>
                                <a href="#" class="btn btn-outline-primary btn-lg btn-block"> View December Offers</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="featured_item_single">
                        <div class="strip grid">
                            <figure>

                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                            </figure>
                            <div class="wrapper">
                                <h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
                                <p>Rs 5.75 - 9.10 Lakh*</p>
                                <a href="#" class="btn btn-outline-primary btn-lg btn-block"> View December Offers</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="featured_item_single">
                        <div class="strip grid">
                            <figure>

                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                            </figure>
                            <div class="wrapper">
                                <h3><a href="detail-restaurant.html">Da Alfredo</a></h3>
                                <p>Rs 5.75 - 9.10 Lakh*</p>
                                <a href="#" class="btn btn-outline-primary btn-lg btn-block"> View December Offers</a>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="sponsored_hero section--grey section--pad" style="background-image:  linear-gradient(to left, rgb(255 255 255 / 0%), rgb(0 0 0)),url('{{ asset('img/static/sponserd_hero.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-12 mt-sm-10 ">

                    <div class="badge badge-success"> Recently Launched</div>
                    <h3>2021 Nissan X-Terra launched </h3>
                    <p>Nissan Middle East has unveiled the 2021 Nissan X-Terra with a web event that marks the return of the popular Xterra nameplate, albeit with a hyphen in the badge and based on the ASEAN-market Terra. </p>
                    <a href="#" class="btn btn-lg btn-primary">Read More</a>


                </div>
            </div>
        </div>
    </section>
    <section class="featured_posts section--pad">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section--title">
                        <h2>Latest Car Updates</h2>
                        <p>Discover a selection of cars with even bigger savings - for a limited time only!</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Car News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Expert Reviews</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Videos</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">The all-new Genesis G80, Leading design and luxury-focused technology</a></h3>
                                                    <p>The All-new Genesis G80 made its Middle East & Africa digital reveal premier. It represents the third generation of the brand’s executive......*</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Ebrahim K. Kanoo launches 14th Toyota Dream Car Art contest</a></h3>
                                                    <p>Ebrahim K. Kanoo, the sole distributor of Toyota vehicles in the Kingdom of Bahrain, has officially launched the 14th edition of the “Toyota......

                                                        *</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Formula 1 update on sustainability progress</a></h3>
                                                    <p>On November 12th, 2019 Formula 1 announced a detailed and ambitious sustainability plan to have a net-zero carbon footprint by 2030. The plan covers:......</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">The all-new Genesis G80, Leading design and luxury-focused technology</a></h3>
                                                    <p>The All-new Genesis G80 made its Middle East & Africa digital reveal premier. It represents the third generation of the brand’s executive......*</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Ebrahim K. Kanoo launches 14th Toyota Dream Car Art contest</a></h3>
                                                    <p>Ebrahim K. Kanoo, the sole distributor of Toyota vehicles in the Kingdom of Bahrain, has officially launched the 14th edition of the “Toyota......

                                                        *</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Formula 1 update on sustainability progress</a></h3>
                                                    <p>On November 12th, 2019 Formula 1 announced a detailed and ambitious sustainability plan to have a net-zero carbon footprint by 2030. The plan covers:......</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">The all-new Genesis G80, Leading design and luxury-focused technology</a></h3>
                                                    <p>The All-new Genesis G80 made its Middle East & Africa digital reveal premier. It represents the third generation of the brand’s executive......*</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Ebrahim K. Kanoo launches 14th Toyota Dream Car Art contest</a></h3>
                                                    <p>Ebrahim K. Kanoo, the sole distributor of Toyota vehicles in the Kingdom of Bahrain, has officially launched the 14th edition of the “Toyota......

                                                        *</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="featured_post_single">
                                        <div class="strip grid">
                                            <figure>

                                                <a href="detail-restaurant.html"><img src="https://imgd.aeplcdn.com/664x374/n/cw/ec/40530/i20-exterior-right-front-three-quarter-5.jpeg" class="img-fluid" alt="" width="400" height="266"></a>

                                            </figure>
                                            <div class="post_info">
                                                <small>Category - 20 Nov. 2017</small>
                                                <h4><a href="detail-restaurant.html">Formula 1 update on sustainability progress</a></h3>
                                                    <p>On November 12th, 2019 Formula 1 announced a detailed and ambitious sustainability plan to have a net-zero carbon footprint by 2030. The plan covers:......</p>
                                                    <a href="#" class=""> View December Offers</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection
