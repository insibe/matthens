
@extends('layouts.app')
@section('content')
<div class="container  my-md-4 ">

   <div class="row my-3">
       <div class="col-lg-9">

           <div class="card">
             <div class="card-header">
                 <h1 class="card-header-title">  {{ ucwords(strtolower($category->title))   }}</h1>
                 @if (Auth::check())
                     <a href="{{ url('/category/'.$category->id.'/edit') }}" class="btn btn-sm btn-outline-secondary">
                         Edit this Category
                     </a>
                 @else
                     <a href="#!" class="btn btn-sm btn-outline-secondary">
                         Claim this business
                     </a>
                 @endif

             </div>
               <div class="card-body">
                   <p> {{ $category->description }}</p>
               </div>

           </div>

           <ul >
               @foreach($businesses as $business)
                   <li>
                       <div class="card">
                           <div class="card-header">
                               <h3 class="card-header-title"> <a href="{{ url('businesses/'.$business->slug) }}">{{ ucwords(strtolower($business->cr_en))   }}</a>  </h3>
                           </div>
                           <div class="card-body">
                               <p>Founded : {{ $business->date_registered }}</p>
                               <p>{{ $business->country }}</p>
                           </div>
                       </div>
                   </li>

               @endforeach


           </ul>
           <p>{{ $businesses->links() }}</p>


       </div>
       <div class="col-lg-3 ">
           <img class="img-fluid" src="https://ph-static.imgix.net/maker_festival_green_earth_sidebar_card.png">
           <div class="col-lg-12 text-center bg-white  app-side-block rounded my-md-4 ">
                    <h4>Gulfthis App</h4>
                         <p> A smarter way to search for the local business in Bahrain.</p>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zeroPadding"> <img class=" img-responsive " data-qk-el-name="imglazyload" src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" data-original-src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" alt="Gulfthis mobile app" title="gulfthis mobile app"> </div>
           </div>
       </div>
   </div>


    <div class="row">

    </div>



</div>
@endsection