@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>Bahrain Indemnity Calculator</h1>
                    <p>People Who Power Our Existance</p>
                </div>
            </div>
        </div>
    </section>
   <section>
       <div class="container  my-md-4 ">

           <div class="row my-3">
               <div class="col-lg-9">
                   <div class="row">
                       <div class="col-lg-12 ">
                           <h2 >How to calculate indemnity in Bahrain </h2>

                           <h3>Salary</h3>
                           <p>The Leaving Indemnity calculated from the last salary the employee received during the time
                               of termination. Which means the Last paid salary considered to calculate the ESB or
                               Indemnity. Some information stating that the indemnity calculated only with Basic Salary.
                               But some companies calculates Basic allowance plus Social allowances, if any. And even some
                               companies providing based on the gross salary. So lets consider it as basic Plus Social
                               allowance.</p>
                           <h3>Indemnity Calculation</h3>
                           <p>
                               Calculating the Leaving indemnity is like Payroll calculation.But it works like the period
                               of employee service. Which means the years and months employee worked in a company. It uses
                               the days also to calculate the ESB. So the last paid salary will be used to calculate the
                               ESB. Lets take an example of calculation.
                           </p>
                           <p> An Employee works for a period of 6 years. and his gross Basic+Social allowance is 700
                               BD(Bahrain Dinar ).
                           </p>
                           First Year Half Salary – 350 BD

                           Second Year Half Salary – 350 BD

                           Third Year Half Salary – 350 BD

                           Forth Year Full Salary – 700 BD

                           Fifth Year Full Salary – 700 BD

                           Sixth Year Full Salary – 700 BD
                           <p>
                               So the Total Indemnity will be calculated as 3150 BD. And if the employee works for 6 years
                               and 7 months and 20 days. The Fraction level Calculation should be applied to get his
                               indemnity. That I have made a calculation for demo.</p>

                           <h3>Conclusion</h3>
                           <p> The calculation and the formulas i used to calculate is may not be matching to all
                               companies. So Consider this calculation is an approximate or tentative values. If you guys
                               have any suggestion or improvement, I will adjust and provide the best results in it.</p>
                       </div>


                   </div>


               </div>
               <div class="col-lg-3 ">
                   <img class="img-fluid" src="https://ph-static.imgix.net/maker_festival_green_earth_sidebar_card.png">
                   <div class="col-lg-12 text-center bg-white  app-side-block rounded my-md-4 ">
                       <h4>Gulfthis App</h4>
                       <p> A smarter way to search for the local business in Bahrain.</p>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zeroPadding"><img class=" img-responsive "
                                                                                             data-qk-el-name="imglazyload"
                                                                                             src="https://assets.quickerala.com/ui/build/images/app-bg-right.png"
                                                                                             data-original-src="https://assets.quickerala.com/ui/build/images/app-bg-right.png"
                                                                                             alt="Gulfthis mobile app"
                                                                                             title="gulfthis mobile app">
                       </div>
                   </div>
               </div>
           </div>


           <div class="row">

           </div>


       </div>
   </section>
@endsection