@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1><a href="#">{{ $title ?? '' }}</a></h1>
        </div>
    </div>

    <div class="container">
        <div class="row">

            @foreach ($posts  as $post)
                <div class="col-3">
                    <div class="card">
                        <div class="card-body">

                            <h2><a href="{{ $post->title }}">{{ $post->title }}</a></h2>
                            <p>{{ $post->excerpt }}</p>
                            <p><small>Posted on {{ $post->created_at }}</small></p>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection