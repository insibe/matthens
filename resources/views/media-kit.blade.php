@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>Media Kit</h1>
                    <p>People Who Power Our Existance</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12">

                            <p>Hello! We’ve created some guidelines to help you use our brand and assets, including our
                                logo, content and trademarks, without having to negotiate legal agreements for each use.
                            <p>
                            <p> To make any use of our marks in a way not covered by these guidelines, please contact us
                                via <a href="#">presskit@gulfthis.com</a> and include a visual mock-up of the intended
                                use.</p>


                        </div>


                    </div>
                </div>


            </div>
            <div class="col-lg-3 ">


            </div>
        </div>
    </section>



@endsection