@extends('layouts.app')
@section('content')
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12  policy-content">

                            <div class="content">
                                <p>BY USING THIS WEBSITE AND OTHER WEBSITES OWNED AND OPERATED BY GULFTHIS&nbsp; (the
                                    “Companies”), YOU AGREE TO BE CONTRACTUALLY BOUND BY THE FOLLOWING TERMS OF USE
                                    AGREEMENT (the “Terms of Use”).&nbsp; IF YOU DO NOT AGREE WITH THESE TERMS OF USE,
                                    DISCONTINUE ALL USE OF THE SITES (AS DEFINED HEREINBELOW) IMMEDIATELY.</p>

                                <p>In these Terms of Use, the term “Sites” refers to any of gulfthis.com or to any other
                                    website owned and operated by the Companies and from which you may have been
                                    redirected.</p>

                                <p>Access to the Sites is offered to you by the Companies’ on the condition that you
                                    accept without modification the terms, conditions and disclaimers contained herein.
                                    Your use of the Sites and the content therein constitutes your agreement to all such
                                    Terms of Use. Any personal information collected, used or disclosed by the Companies
                                    through the Sites is subject to the Companies’ Privacy Policy. By agreeing to these
                                    Terms of Use, you consent to the collection, use and disclosure of personal
                                    information in accordance with our Privacy Policy.</p>

                                <p>The Terms and Conditions of Use related to your Account, as defined herein below,
                                    form an integral part of, and are incorporated by reference to, these Terms of Use
                                    and can be accessed by clicking here.</p>

                                <p>&nbsp;</p>

                                <p><strong>1. Non-Commercial Use</strong></p>

                                <p>The Sites are for personal and non-commercial use. All content included in the Sites,
                                    including but not limited to text, information, designs, photographs, graphics,
                                    images, illustrations, interfaces, codes, audio clips, video clips, software and
                                    links to external websites (the "Content"), is provided for informational purposes
                                    only.<br>
                                    &nbsp;</p>

                                <p><strong>2. Warranty Disclaimer</strong></p>

                                <p>The Companies intend for the Content on the Sites to be accurate and reliable,
                                    however, the Sites and the Content are provided to you on an "as is" and "as
                                    available" basis and without warranty or condition of any kind, whether express or
                                    implied. In particular, the Companies do not control the Content posted by users on
                                    the Sites and make no representation or warranty whatsoever regarding the accuracy
                                    or completeness of any data or information accessible on or through the Sites.
                                    Furthermore, to the fullest extent permissible pursuant to applicable law, the
                                    Companies, their respective affiliates, and their respective officers, directors,
                                    employees, affiliates, suppliers, advertisers, representatives and agents disclaim
                                    all warranties and conditions, express, implied, legal or statutory, including, but
                                    not limited to, implied warranties of title, quality, non-infringement, freedom from
                                    computer viruses, warranties arising from course of dealing or course of
                                    performance, merchantable quality and fitness for a particular purpose. The
                                    Companies expressly disclaim any representation or warranty that the Sites will be
                                    free from errors, viruses or other harmful components, that communications to or
                                    from the Sites will be secure and not intercepted, that the services and other
                                    capabilities offered from the Sites will be uninterrupted, or that its content will
                                    be accurate, complete, adequate or timely. Any material downloaded is at your own
                                    risk and you will be solely responsible for any damage to your computer system or
                                    loss of data that results from the download of any such material.</p>

                                <p>The Sites offer various forms of advice, including, general, legal, health, fitness
                                    and nutritional information and are designed for entertainment, informational and
                                    educational purposes only. You should not rely on this information as a substitute
                                    for, nor does it replace, professional advice. If you have any concerns or questions
                                    about your health, you should always consult with a health care professional. Do not
                                    disregard, avoid or delay obtaining professional advice, including medical or health
                                    related advice from your health care professional because of something you may have
                                    read on this site. The use of any information provided on this site is solely at
                                    your own risk.</p>

                                <p>Developments in technology, professional practices and medical research may impact
                                    the advice that appears on the Sites. No assurance can be given that the advice
                                    contained in the Sites will always include the most recent findings or developments
                                    with respect to the particular material.<br>
                                    &nbsp;</p>

                                <p><strong>3. Liability Disclaimer</strong></p>

                                <p>Without limiting the foregoing, the Companies expressly exclude (and you hereby
                                    release the Companies from) any liability for any of the following:</p>

                                <p>fraud by any user of the Sites;</p>

                                <p>any misrepresentation by a third party (whether innocent or fraudulent) made in
                                    respect of the Sites and/or the content therein;</p>

                                <p>any failure by the Companies to ensure that they do not breach any copyright or other
                                    intellectual property right of any third party;</p>

                                <p>any link on the Sites to any other site; and</p>

                                <p>loss or damage caused by delay or errors in, or the downtime of, the Site (or
                                    servers) or resulting from interruption, termination, or failed operation of the
                                    Internet or a third-party telecommunication service, even in the event that the
                                    Companies have been advised of the possibility of such loss or damage.</p>

                                <p>Nothing in these Terms of Use is intended to limit or exclude any liability on the
                                    part of the Companies where and to the extent that applicable laws prohibit such
                                    exclusion or limitation.</p>

                                <p>In no event shall the Companies or its affiliates, licensors, suppliers, advertisers,
                                    agents or sponsors, be responsible or liable for any indirect, incidental,
                                    consequential, special, exemplary, punitive or other damages under any contract,
                                    negligence, tort, extra-contractual liability, strict liability or other theory,
                                    arising out of or in connection with the use or performance of the Sites or any
                                    other site you or users of your account may access while using the Sites or your use
                                    or the use by users of your account of the Content, even if advised of the
                                    possibility of such damages.<br>
                                    &nbsp;</p>

                                <p><strong>4. Dealing with Third Parties</strong></p>

                                <p>Any correspondence or business dealings with any third parties including merchants,
                                    sellers, buyers or advertisers found on, or through, the Sites is solely between you
                                    and such third parties, and the Companies have no control over the quality, legality
                                    or inappropriate nature of the Content advertised, the truth or accuracy of any
                                    representations made by sellers, the ability of sellers to sell and the ability of
                                    buyers to purchase. The Companies are not responsible for any loss or damages you
                                    may suffer by entering into such transactions including the payment for and delivery
                                    of goods if any, and any terms, conditions, warranties, or representations
                                    associated with such dealings. You and the third party, not the Companies, are
                                    responsible for compliance with all laws applicable in any such transaction.<br>
                                    &nbsp;</p>

                                <p><strong>5. Indemnity.</strong></p>

                                <p>You acknowledge and expressly agree that use of the Sites is at your sole and own
                                    risk. You agree to defend, indemnify and hold the Companies and their affiliates as
                                    well as their respective directors, officers, trustees and employees harmless from
                                    any and all liabilities, costs and expenses, including reasonable attorneys' fees,
                                    related to any violation of these Terms of Use by you or users of your account, or
                                    in any way arising out of the use of the Sites, including without limitation, the
                                    placement or transmission of any information or other materials on the Sites by you
                                    or users of your account.<br>
                                    &nbsp;</p>

                                <p><strong>6. Prohibited Use of the Sites.</strong></p>

                                <p>You shall not use the Sites in any way that would interfere with their operation nor
                                    submit any content to the Sites which libels, defames, invades privacy, is obscene,
                                    pornographic, abusive or threatening, infringes intellectual property laws or
                                    violates any other applicable laws. You shall not post, upload, publish, transmit or
                                    otherwise distribute on or through the Sites any information or other materials
                                    that:</p>

                                <p>could constitute a criminal offence, including without limitation, any crimes
                                    relating to pornography, threats, intimidation, hate, racism, assault, or fraud;</p>

                                <p>could defame, abuse, harass, threaten or otherwise interfere with or harm the
                                    contractual, personality, confidentiality, privacy, publicity, moral or statutory or
                                    any other rights of any person, including, without limitation and for greater
                                    certainty, the Companies and their respective affiliates;</p>

                                <p>could infringe the intellectual property rights including, without limitation, any
                                    copyright, trade-mark, or patent, of any person, including, without limitation and
                                    for greater certainty, the Companies and their affiliates;</p>

                                <p>could be considered as a use of the Sites that is contrary to law or electronic
                                    etiquette, or which would adversely impact the use of the Sites or the Internet by
                                    other users, including the posting or transmitting of information or software
                                    containing viruses or other disruptive components;</p>

                                <p>contain any distasteful or offensive material including, but not limited to, material
                                    intended for an adult audience;</p>

                                <p>contain: (i) falsehoods or misrepresentations (ii) commercial content, including
                                    without limitation, advertising, solicitation, offers to sell any products, goods or
                                    services,&nbsp; or (iii) unsolicited or unauthorized advertising, promotional
                                    materials, “junk mail,” “spam,” “chain letters,” “pyramid schemes,” or any other
                                    form of solicitation, which prohibition includes but is not limited to (a) using the
                                    Sites to send messages to people who don’t know you or who are unlikely to recognize
                                    you as a known contact or who have not consented to receiving electronic messages
                                    from you in accordance with applicable anti-spam legislation; (b) using the Sites to
                                    connect to people who don’t know you and then sending unsolicited promotional
                                    messages to those direct connections without their permission; and (c) sending
                                    messages to distribution lists, newsgroup aliases, or group aliases; or (iii)
                                    surveys or contests; or</p>

                                <p>encourage, conspire, entice or promote the occurrence of any of the prohibited
                                    conduct stipulated herein.</p>

                                <p>The Companies do not endorse any submission or any opinion, recommendation or advice
                                    that you may submit on or through the Sites and the Companies expressly disclaim any
                                    and all liability in connection with your submissions to the Sites.</p>

                                <p>The Companies reserve the right to remove any information that you post, upload,
                                    publish, transmit, distribute or otherwise use on the Sites, in whole or in part, at
                                    any time, at its sole discretion and without prior notice.<br>
                                    &nbsp;</p>

                                <p><strong>7. License</strong></p>

                                <p>By posting, uploading or submitting any information, material or content to the
                                    Sites, you automatically grant (or warrant that the owner of the rights to such
                                    material or content has expressly granted you the rights to grant) the Companies a
                                    non-exclusive, irrevocable, worldwide, perpetual, unlimited, assignable,
                                    sublicensable, royalty-free right to use, reproduce, modify, adapt, improve, create
                                    derivative works from, publish, remove, retain, add, process, analyze, translate,
                                    license, transmit, distribute and otherwise exploit any or all portions of such
                                    information, material or content in any manner and media and by means of any
                                    technology now known or hereafter developed. In addition, you hereby irrevocably
                                    waive all moral rights in any such information, material or content posted, uploaded
                                    or submitted by you.<br>
                                    &nbsp;</p>

                                <p><strong>8. Web Scraping/Harvesting</strong></p>

                                <p>You may only use or reproduce the Content for your own personal and non-commercial
                                    use. The framing, scraping, data-mining, extraction or collection of the Content of
                                    the Sites in any form and by any means whatsoever is strictly prohibited.
                                    Furthermore, you may not mirror any material contained on this Sites.<br>
                                    &nbsp;</p>

                                <p><strong>9. Hyperlinking to the Sites</strong></p>

                                <p>You may create a link (the "Hyperlink") to one or more of the Sites provided
                                    that:</p>

                                <p>you do not replicate any of the Content of the Sites;</p>

                                <p>you do not create a frame or any other bordered environment around the Sites or the
                                    Content contained in the Sites;</p>

                                <p>the Hyperlink shall not imply any endorsement of any products, goods or services;
                                    and</p>

                                <p>the website linking to the Sites does not contain any information, content or
                                    material that would interfere with the operation of the Sites nor which libels,
                                    defames, invades privacy, is obscene, pornographic, abusive or threatening,
                                    infringes intellectual property laws or violates any other applicable laws.</p>

                                <p>The Companies may withdraw your right to hyperlink to the Sites at any time in their
                                    sole discretion.<br>
                                    &nbsp;</p>

                                <p><strong>10. Links to Third Party Websites</strong></p>

                                <p>Links available on the Sites will allow you to link to websites or resources not
                                    maintained or controlled by the Companies. The Companies provide these links for
                                    your convenience only and make no endorsements, warranties or representations of any
                                    kind whatsoever regarding those other websites and expressly disclaim all
                                    responsibility for content on third party websites. The Companies have neither
                                    reviewed nor approved any content that appears on such other websites. Please be
                                    aware that you link to these third party websites at your own risk. You acknowledge
                                    and agree that the Companies shall not be held responsible for the legality,
                                    accuracy or inappropriate nature of any content, advertising, products, services, or
                                    information located on or through any other websites, nor for any loss or damages
                                    caused or alleged to have been caused by the use of or reliance on any such content.<br>
                                    &nbsp;</p>

                                <p><strong>11. Copyright</strong></p>

                                <p>All Content published on or otherwise accessible through the Sites, including without
                                    limitation, all texts, designs, images, photographs and other information, is
                                    protected under copyright laws to the maximum extent permitted.
                                    The Content, and the copyright in the Content, are owned or controlled by the
                                    Companies and/or its affiliates, licensors, suppliers, representatives or agents
                                    (each a “Copyright Holder”). The Companies grant you a limited license to display or
                                    print the Content for your own personal and non-commercial use only. You may not
                                    modify, copy, distribute, transmit, display, perform, reproduce, publish, license,
                                    create derivative works from, transfer or sell these materials in any form or manner
                                    without the express written permission of the Copyright Holder(s) of the Content.
                                    You must abide by all copyright notices, information and restrictions contained in
                                    any Content on, or accessed through, the Sites and maintain such notices in the
                                    Content. The Companies do not warrant or represent that your use of materials
                                    displayed on, or obtained through, the Sites will not infringe the rights of third
                                    parties.<br>
                                    &nbsp;</p>

                                <p><strong>12. Trademarks</strong></p>

                                <p> All other products, services and company names mentioned in the Sites may be
                                    trademarks of either of the Companies and/or their affiliates or their respective
                                    owners. You shall not use any trademark displayed on the Sites without the express
                                    written permission of the applicable Company or the relevant owner of the
                                    trademark.</p>

                                <p><strong>13. Changes and updates</strong></p>

                                <p>The Companies reserve the right to change or remove any Content from the Sites, in
                                    whole or in part, at their sole discretion, at any time, without notice. The
                                    Companies reserve the exclusive right to modify these Terms of Use at any time.
                                    Non-material changes and clarifications will take effect immediately. Any material
                                    changes will take effect 30 days after their posting. You must regularly review
                                    these Terms of Use. Please refer to the last update date which appears at the bottom
                                    of this page to know the posting date of any change. Your continued use of the Sites
                                    following any modification to the Terms of Use constitutes your agreement to such
                                    modified Terms of Use.<br>
                                    &nbsp;</p>

                                <p><strong>14. Registered User Accounts</strong></p>

                                <p>In order to access certain parts of the Sites, you may have to register as a user
                                    with the Companies. During registration for a user account (“Account”), you will
                                    select a password and account name. You understand and agree that you are solely
                                    responsible for maintaining the confidentiality of your account including your
                                    password, and are fully responsible for all activities that occur under your
                                    account. You agree to (a) immediately notify the Companies of any unauthorized use
                                    of your password or account or any other breach of security, and (b) log out from
                                    your account at the end of each session. The Companies will not be liable for any
                                    loss or damage arising from your failure to comply with this section.<br>
                                    &nbsp;</p>

                                <p><strong>15. Applicable Law</strong></p>

                                <p>These Terms of Use shall be governed by and construed in accordance with the laws of
                                    Québec and the federal laws of Canada applicable therein. The parties hereby agree
                                    and attorn to the exclusive jurisdiction of the courts of the province in which you
                                    reside. The parties have required that these Terms of Use and all related documents
                                    be drawn up in English. Les parties ont demandé que ces termes et conditions ainsi
                                    que tous les documents qui s'y rattachent soient rédigés en anglais.<br>
                                    &nbsp;</p>

                                <p><strong>16. General</strong></p>

                                <p>The Companies reserve the right to terminate or suspend access to the Sites and/or to
                                    terminate these Terms of Use at any time without notice and for any reason
                                    whatsoever. The Companies reserve the right at all times to disclose any information
                                    as necessary to satisfy any applicable law, regulation, legal process or
                                    governmental request, or to edit, refuse to post or to remove any information or
                                    materials, in whole or in part, in their sole discretion. Any service order or
                                    transaction performed on or through the Sites may be subject to additional terms and
                                    conditions or terms of sale. The Terms of Use of the Sites and all other documents
                                    referenced herein constitute the entire agreement between the Companies and you with
                                    respect to your use of the Sites or the use by users of your account. The Companies'
                                    failure to insist upon, or enforce strict performance, of any provision of these
                                    Terms of Use shall not be construed as a waiver of any provision or right. If any
                                    terms in these Terms of Use are determined to be void, invalid or otherwise
                                    unenforceable by a court of competent jurisdiction, such determination shall not
                                    affect the remaining provisions hereof.<br>
                                    &nbsp;</p>


                                <p>&nbsp;</p>

                                <p style="text-align: center;"><strong>MESSAGES TERMS OF USE</strong></p>

                                <p>&nbsp;</p>

                                <p>The following terms of use (the “Messages Terms of Use”), in addition to the <a
                                            href="https://corporate.yp.ca/en/legal-notice/terms-of-use-agreement/"
                                            target="&quot;_blank&quot;">Terms of Use Agreement</a>, shall apply to the
                                    user (hereinafter “you”) when using the messaging functionality operated by the
                                    Companies (the “Messages Functionality”). In the event of conflict between the
                                    Messages Terms of Use and the Terms of Use Agreement, the Messages Terms of Use
                                    shall prevail. Your use of the Messages Functionality shall constitute your
                                    acceptance of these Messages Terms of Use and of the Terms of Use Agreement.</p>

                                <p><strong>1. Electronic Communications</strong></p>

                                <p>By using the Messages Functionality to send communications to a recipient, you
                                    consent to receive certain communications in connection with the Messages
                                    Functionality, including but not limited to notifications and other commercial
                                    electronic communications from the recipient of your communications or from the
                                    Companies on behalf of such recipient.</p>

                                <p><strong>2.&nbsp;Expiration</strong></p>

                                <p>All of the content and information (the “Content”) sent through the Messages
                                    Functionality will be archived and accessible for a period of two (2) years
                                    following the date on which it is sent. At the expiration of such period, the
                                    Companies will automatically and permanently remove such expired Content.</p>

                                <p><strong>3. User information</strong></p>

                                <p>In order to use the Messages Functionality, you must provide certain information
                                    about yourself (your “Information”). You understand and agree that you are solely
                                    responsible for maintaining the confidentiality of your Information, and are fully
                                    responsible for all activities that occur as a result of the use of your
                                    Information. You agree to immediately notify the Companies of any unauthorized use
                                    of your Information or any other breach of security. The Companies will not be
                                    liable for any loss or damage arising from your failure to comply with this
                                    section.</p>

                                <p><strong>4. Content</strong></p>

                                <p>You own all of the Content that you submit or transmit through the Messages
                                    Functionality. The Companies may only use such Content in accordance with the
                                    Companies’ <a href="https://corporate.yp.ca/en/legal-notice/privacy-statement/"
                                                  target="&quot;_blank&quot;">Privacy&nbsp;Policy</a>. Notwithstanding
                                    the foregoing, you acknowledge and agree that the Companies shall have all of the
                                    rights necessary to: (i) transmit the Content to the selected recipient; and (ii)&nbsp;archive
                                    the Content in accordance with section 2 hereof.</p>

                                <p><strong>5. Prohibited use of the message functionnality&nbsp;</strong></p>

                                <p>You shall not use the Messages Functionality in any way that would interfere with its
                                    operation nor submit any content to the Messages Functionality which libels,
                                    defames, invades privacy, is obscene, pornographic, abusive or threatening,
                                    infringes intellectual property laws or violates any other applicable laws. You
                                    shall not post, upload, publish, transmit or otherwise distribute on or through the
                                    Messages Functionality any information or other materials that:<br>
                                    &nbsp;</p>

                                <ul>
                                    <li><p>could constitute a criminal offence, including without limitation, any crimes
                                            relating to pornography, threats, intimidation, hate, racism, assault, or
                                            fraud;</p></li>

                                    <li><p>could defame, abuse, harass, threaten or otherwise interfere with or harm the
                                            contractual, personality, confidentiality, privacy, publicity, moral or
                                            statutory or any other rights of any person, including, without limitation
                                            and for greater certainty, the Companies and their respective affiliates;</p>
                                    </li>

                                    <li><p>could infringe the intellectual property rights including, without
                                            limitation, any copyright, trade-mark, or patent, of any person, including,
                                            without limitation and for greater certainty, the Companies and their
                                            affiliates;</p></li>

                                    <li><p>could be considered as a use of the Messages Functionality that is contrary
                                            to law or electronic etiquette, or which would adversely impact the use of
                                            the Messages Functionality or the Internet by other users, including the
                                            posting or transmitting of information or software containing viruses or
                                            other disruptive components;</p></li>

                                    <li><p>&nbsp;contain any distasteful or offensive material including, but not
                                            limited to, material intended for an adult audience;</p></li>

                                    <li><p>contain: (a) falsehoods or misrepresentations or (b) unsolicited or
                                            unauthorized advertising, promotional materials, “junk mail”, “spam”, “chain
                                            letters”, “pyramid schemes”, or any other form of solicitation, which
                                            prohibition includes but is not limited to using the Messages Functionality
                                            to send messages to users who have not consented to receiving commercial
                                            electronic messages from you in accordance with applicable anti-spam
                                            legislation; or (c) surveys or contests; or</p></li>

                                    <li><p>>encourage, conspire, entice or promote the occurrence of any of the
                                            prohibited conduct stipulated herein.<br>
                                            &nbsp;</p</li>
                                </ul>

                                <p>The Companies do not endorse any submission or any opinion, recommendation or advice
                                    that you may submit on or through the Messages Functionality and the Companies
                                    expressly disclaim any and all liability in connection with your submissions to the
                                    Messages Functionality.</p>

                                <p>The Companies reserve the right to block your access to the Messages Functionality at
                                    any time, at its sole discretion and without prior notice.</p>

                            </div>
                        </div>


                    </div>
                </div>


            </div>
            <div class="col-lg-3 ">


            </div>
        </div>
    </section>





@endsection