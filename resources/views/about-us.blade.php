@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff  ">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>About Gulfthis</h1>
                    <p>Bahrain's No.1 Local Search Engine</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <p><a href="https://gulfthis.com/">Gulfthis.com</a> is a one-stop destination in effect a
                                guide map on the <a href="{{ url('about-bahrain') }}">Kingdom of Bahrain</a> . A
                                comprehensive collection of all the information about the island. You are just a click
                                away from all the answers to your queries. With <span>gulfthis</span> on your side, you
                                will now be updated about every activity in the Kingdom of Bahrain.
                            </p>
                            <p>We offer an exhaustive high-quality database on anything and everything you want to know.
                                Our algorithm will make your life easier by narrowing your search to apt information in
                                a matter of seconds. We help our users connect to the right providers of products and
                                services and at the same time, we aid the business people to reach their targeted
                                markets.</p>
                            <p>We are an ardent technology-driven platform focussing on helping people access valid
                                information, news, and guides without any hassle. We curate information from different
                                resources, verify and validate the authenticity before publishing it on the platform.
                                Our team of talented developers and curators work tirelessly to bring a user-friendly
                                interface with reliable information.
                            </p>
                            <p>So what are you waiting for? <a alt="gulfthis"
                                                               href="https://gulfthis.com/">Gulfthis.com</a> is just a
                                click away! </p>


                        </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-3 ">


            </div>
        </div>

    </section>
@endsection