@extends('layouts.app')

@section('content')




{{--    <!-- =======================--}}
{{--    Main content START -->--}}
{{--    <section class="position-relative">--}}
{{--        <div class="container" data-sticky-container>--}}
{{--            <div class="row">--}}
{{--                <!-- Main Post START -->--}}
{{--                <div class="col-lg-12">--}}
{{--                    <!-- Top highlights START -->--}}

{{--                @if(count($posts ) > 0)--}}
{{--                    @foreach($posts as $post)--}}
{{--                    <!-- Card item START -->--}}
{{--                    <div class="card border rounded-3 up-hover p-3 m mb-4">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-4">--}}
{{--                                <img class="rounded-3" src="{{ Storage::url($post->featured_image) }}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="col-md-8 mt-3 mt-md-0">--}}
{{--                                @foreach($post->categories  as $category)--}}
{{--                                    <a href="{{ url('category/'.$category->slug) }}" class="badge bg-info mb-2">{{ $category->title }}</a>--}}
{{--                                @endforeach--}}

{{--                                <h4>{{ $post->title }}</h4>--}}
{{--                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block">--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <div class="nav-link">--}}
{{--                                                <div class="d-flex align-items-center position-relative">--}}
{{--                                                    <span class="small">Instant News by  {{ $post->user->name }} /  {{ \Carbon\Carbon::parse($post->created_at)->format('h:m a')}}  on  {{ \Carbon\Carbon::parse($post->created_at)->format('d M Y,l')}} </span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                <p> {{ $post->excerpt }}</p>--}}
{{--                                <!-- Card info -->--}}
{{--                               <p> read more at  <a href="{{ $post->post_url }}"> <strong>{{ $post->feed->title}}</strong> </a></p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Card item END -->--}}
{{--                    @endforeach--}}
{{--                        <button type="button" class="btn btn-primary-soft w-100">Load more post <i class="bi bi-arrow-down-circle ms-2 align-middle"></i></button>--}}
{{--                @else--}}

{{--                @endif--}}

{{--                    <!-- Adv -->--}}
{{--                    <div>--}}
{{--                        <a href="#" class="card-img-flash d-block mt-4">--}}
{{--                            <img src="theme/images/adv-1.png" alt="adv">--}}
{{--                        </a>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--                <!-- Main Post END -->--}}

{{--            </div> <!-- Row end -->--}}
{{--        </div>--}}
{{--    </section>--}}
{{--    <!-- =======================--}}
{{--    Main content END -->--}}






    <section class="heroBg" style="background: url('{{ asset('images/buildings.svg') }}') no-repeat bottom center  #0051d4  " >
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h2 class="headline">Bahrain's No.1 Local Search Engine</h2>
                    <p class="subline">Company reviews. Salaries. Interviews. Jobs.</p>
                    <div class="col-md-6 col-md-offset-3">
                        <form  action="{{ route('search') }}" method="GET">

                            <div class="input-group input-group-lg">
                                <input class="form-control form-control-lg" name="search" placeholder="Search for anything, anywhere in Bahrain" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Go</button> </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>



                </div>

                <div class="row mt-3">
                    <div class="col-sm-12">
                        <a href="#">Web Design Company In Bahrain</a> |
                        <a href="#">Attractions</a> |
                        <a href="#">Stories</a> |
                        <a href="#">Guides</a>

                    </div>
                </div>
                <!-- row -->
            </div>

        </div>


    </section>
    <!-- /hero_single -->
    <section>
        <div class="container">
            <div class="row">
                <div class="popular_categories">
                    <div class="col-lg-12">
                        <h3>Popular Categories</h3>
                        <ul>
                            @if(count($categoriesList ) > 0)
                                @foreach($categoriesList as $category)
                                    <li >
                                        <a href="{{ url('/category/'.$category->slug ) }}">{{ $category->title }} Companies in Bahrain</a>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <a class="#">Nothing Found</a>
                                </li>
                            @endif

                        </ul>
                    </div>
                    <div class="col-5">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h2 class=" text-primary card-header-title">Newsletter</h2>
                        </div>

                        <div class="card-body">
                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                            <form  action="{{ route('search') }}" method="GET">

                                <div class="input-group input-group-lg">
                                    <input class="form-control form-control-lg" name="search" placeholder="Your email address" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">
                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Subscribe</button> </span>
                                </div>
                                <!-- /input-group -->
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                   <div class="card">
                       <div class="card-header">
                           <h2 class=" text-primary card-header-title">Companies in Bahrain</h2>
                       </div>

                       <div class="card-body">
                           <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>
                           <a href="{{ url('/') }}" class="btn btn-primary btn-lg  me-2">View All Businesses</a>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Over  {{ \App\Models\Business::whereIn('cr_status', array('ACTIVE'))->count()  }} companies</h1>
                    <p>Get in depth information about companies. Know about their mission, vision, products, services and much more.</p>
                </div>
                <div id="companiesWrap">
                    @if(count($businesses ) > 0)
                        @foreach($businesses as $business)
                        <div class="company_item" ><a href="{{ url('/businesses/'.$business->slug ) }}" title="{{ $business->title }}"><img class="company_logo" src="https://static.ambitionbox.com/static/icons/company-placeholder.svg" onerror="this.onerror=null;this.src='https://static.ambitionbox.com/static/icons/company-placeholder.svg';" alt="{{ $business->title  }}"></a></div>



                        @endforeach
                    @else
                        <li>
                            <a class="#">Nothing Found</a>
                        </li>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h2>Bahrain News & Updates</h2>
                </div>

                @if(count($posts ) > 0)
                    @foreach($posts as $post)
                        <div class="col-lg-4">
                            <div class="card">
                                <!-- Card img -->
                                <div class="position-relative">
                                    <img class="card-img" src="{{ asset('https://www.cibjo.org/wp-content/uploads/2019/04/Four-Seasons-1-lr.jpg')  }}" alt="Card image">
                                    <div class="card-img-overlay d-flex align-items-start flex-column p-3">
                                        <!-- Card overlay Top -->
                                        <div class="w-100 mb-auto d-flex justify-content-end">
                                            <div class="text-end ms-auto">
                                                <!-- Card format icon -->
                                                <div class="icon-md bg-white-soft bg-blur text-white rounded-circle" title="This post has video"><i class="fas fa-video"></i></div>
                                            </div>
                                        </div>
                                        <!-- Card overlay bottom -->
                                        <div class="w-100 mt-auto">
                                            <!-- Card category -->
                                            <a href="#" class="badge bg-danger mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Travel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"><a href="{{ $post->slug }}" class="btn-link text-reset fw-bold">{{ $post->title }}</a></h4>
                                    <p class="card-text">{{ $post->excerpt }}</p>
                                    <!-- Card info -->
                                    <ul class="nav nav-divider align-items-center d-none d-sm-inline-block">
                                        <li class="nav-item">
                                            <div class="nav-link">
                                                <div class="d-flex align-items-center position-relative">
                                                    <div class="avatar avatar-xs">
                                                        <img class="avatar-img rounded-circle" src="assets/images/avatar/02.jpg" alt="avatar">
                                                    </div>
                                                    <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Dennis</a></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="nav-item">{{ $post->published_at }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>



                    @endforeach
                @else
                    <li>
                        <a class="#">Nothing Found</a>
                    </li>
                @endif


            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Why Gulfthis Community</h2>

                </div>

               6
            </div>
        </div>

    </section>

@endsection
