<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" id="viewport"content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=yes, shrink-to-fit=no, minimal-ui, viewport-fit=cover">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="https://gulfthis.com/" itemprop="url">
    <meta name="google-site-verification" content="UOlx-Os2VCgYw_4GazZB1CeHRYomE-tLDFJVY2DSMPw"/>
    <title itemprop="name">@yield('title','Gulfthis | Bahrain  No.1 Local Search Engine & business listings service')</title>
    <meta name="keywords" content="@yield('meta_keywords','Bahrain, Bahrain News')">
    <meta name="description"
          content="@yield('meta_description','Gulfthis, Bahrain local search engine, Gulfthis curates Social content, News, Videos & more from Top Publishers on all Trending Topics.')">
    <link rel="canonical" href="{{url()->current()}}"/>

    <meta property="og:url" content="@yield('ogUrl','https://www.gulfthis.com/finance/gold-rate-in-bahrain/') "/>
    <meta property="og:title" content=" @yield('ogTitle','Gold Rate in Kingdom of Bahrain - GulfThis')"/>
    <meta property="og:description" content="@yield('ogDescription','Check latest gold rate in bahrain in indian rupees and bahraini dinar per gram, tola, sovereign, ounce and kilogram. 24k, 22k, 21k, 18k gold rate in kingdom of bahrain, gold bars, gold biscuits and gold coins prices in bahrain today.') "/>
    <meta property="og:image" content="@yield('ogImage','https://www.goldenchennai.com/finance/images/gold/gold-rate-in-bahrain.gif')"/>

    {{--    <meta property='article:published_time' content='2015-01-31T20:30:11-02:00' />--}}
    {{--    <meta property='article:section' content='news' />--}}

    {{--    <meta property="og:description"content="description..." />--}}
    {{--    <meta property="og:title"content="Title" />--}}
    {{--    <meta property="og:url"content="http://current.url.com" />--}}
    {{--    <meta property="og:type"content="article" />--}}
    {{--    <meta property="og:locale"content="pt-br" />--}}
    {{--    <meta property="og:locale:alternate"content="pt-pt" />--}}
    {{--    <meta property="og:locale:alternate"content="en-us" />--}}
    {{--    <meta property="og:site_name"content="name" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img1.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img2.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img3.jpg" />--}}
    {{--    <meta property="og:image:url"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image:size"content="300" />--}}

    {{--    <meta name="twitter:card"content="summary" />--}}
    {{--    <meta name="twitter:title"content="Title" />--}}
    {{--    <meta name="twitter:site"content="@LuizVinicius73" />--}}

    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <!-- OR with multi -->--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebPage","name":"Title - Over 9000 Thousand!"}</script>--}}


    <meta name="format-detection" content="telephone=no">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="x-rim-auto-match" content="none">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <meta name="msapplication-TileImage" content="images/app-icons/icon-152x152.png">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Progressive Web Application">
    <meta name="application-name" content="gulfthis">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }} " type="image/x-icon"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('theme/images/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('theme/images/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('theme/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('theme/images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('theme/images/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('theme/images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('theme/images/apple-touch-icon-144x144.png') }}/">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('theme/images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('theme/images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/android-chrome-192x192.png') }}" sizes="192x192">
    <meta name="msapplication-square70x70logo" content="{{ asset('theme/images/smalltile.png') }}"/>
    <meta name="msapplication-square150x150logo" content="{{ asset('theme/images/mediumtile.png') }}"/>
    <meta name="msapplication-wide310x150logo" content="{{ asset('theme/images/widetile.png') }}"/>
    <meta name="msapplication-square310x310logo" content="{{ asset('theme/images/largetile.png') }}"/>

    <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/bootstrap-icons/bootstrap-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/tiny-slider/tiny-slider.css') }} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/glightbox/css/glightbox.css') }}">

    <!-- Theme CSS -->
    <link id="style-switch" rel="stylesheet" type="text/css" href="{{ asset('theme/css/style.css') }} ">

</head>

<body>

<!-- Offcanvas START -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasMenu">
    <div class="offcanvas-header">
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body d-flex flex-column pt-0">
        <div>
            <img class="light-mode-item my-3" src="{{ asset('theme/images/gulfthis-malayalam.svg') }}" alt="logo">
            <img class="dark-mode-item my-3" src="{{ asset('theme/images/gulfthis-malayalam.svg') }} " alt="logo">
            <p>The next-generation blog, news, and magazine theme for you to start sharing your stories today! </p>
            <!-- Nav START -->
            <ul class="nav d-block flex-column my-4">
                <li class="nav-item">
                    <a href="index.html" class="nav-link h5 d-block">Home</a>
                <li class="nav-item">
                    <a class="nav-link h5" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link h5" href="#">Our Journal</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link h5" href="#">Contact Us</a>
                </li>
            </ul>
            <!-- Nav END -->
            <div class="bg-primary-soft p-4 mb-4 text-center w-100 rounded">
                <span>The Blogzine</span>
                <h3>Save on Premium Membership</h3>
                <p>Get the insights report trusted by experts around the globe. Become a Member Today!</p>
                <a href="#" class="btn btn-warning">View pricing plans</a>
            </div>
        </div>
        <div class="mt-auto pb-3">
            <!-- Address -->
            <p class="text-body mb-2 fw-bold">New York, USA (HQ)</p>
            <address class="mb-0">750 Sing Sing Rd, Horseheads, NY, 14845</address>
            <p class="mb-2">Call: <a href="#" class="text-body"><u>469-537-2410</u> (Toll-free)</a> </p>
            <a href="#" class="text-body d-block">hello@blogzine.com</a>
        </div>
    </div>
</div>
<!-- Offcanvas END -->

<!-- =======================
Header START -->
<header>
    <!-- Navbar top -->
    <div class="navbar-top d-none d-lg-block">
        <div class="container">
            <div class="row d-flex align-items-center my-2">
                <!-- Top bar left -->
                <div class="col-sm-8 d-sm-flex align-items-center">
                    <!-- Title -->
                    <div class="me-3">
                        <span class="badge bg-primary p-2 px-3 text-white">Trending:</span>
                    </div>
                    <!-- Slider -->
                    <div class="tiny-slider arrow-end arrow-xs arrow-bordered arrow-round arrow-md-none">
                        <div class="tiny-slider-inner"
                             data-autoplay="true"
                             data-hoverpause="true"
                             data-gutter="0"
                             data-arrow="true"
                             data-dots="false"
                             data-items="1">
                            <!-- Slider items -->
                            <div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think</a></div>
                            <div> <a href="#" class="text-reset btn-link">How the 10 worst business fails of all time could have been prevented </a></div>
                            <div> <a href="#" class="text-reset btn-link">The most common business debate isn't as black and white as you might think </a></div>
                        </div>
                    </div>
                </div>

                <!-- Top bar right -->
                <div class="col-sm-4">
                    <div class="languages">
                        @foreach(config()->get('app.locales') as $code => $lang)
                            <a href="http://{{$code}}.gulfthis.com">{{ $lang }}</a>
                        @endforeach
                    </div>
                    <ul class="list-inline mb-0 text-center text-sm-end">
                        <li class="list-inline-item">
                            <span>Wed, March 31, 2021</span>
                        </li>
                        <li class="list-inline-item">
                            <i class="bi bi-cloud-hail text-info"></i>
                            <span>13 °C NY, USA</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Divider -->
            <div class="border-bottom border-2 border-primary opacity-1"></div>
        </div>
    </div>
    <!-- Navbar logo section START -->
    <div>
        <div class="container">
            <div class="d-sm-flex justify-content-sm-between align-items-sm-center my-2">
                <!-- Logo START -->
                <a class="navbar-brand d-block" href="{{ url('/') }}">
                    <img class="navbar-brand-item light-mode-item" src="{{ asset('theme/images/gulfthis-malayalam.svg') }}" alt="logo" style="height: 46px;">
                    <img class="navbar-brand-item dark-mode-item" src="{{ asset('theme/images/gulfthis-malayalam.svg') }}" alt="logo" style="height: 46px;">
                </a>
                <!-- Logo END -->
                <!-- Adv -->
                <div>
                    <a href="#" class="card-img-flash d-block">
                        <img src="{{ asset('theme/images/adv-2.png') }}" alt="adv">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Navbar logo section END -->

    <!-- Navbar START -->
    <div class="navbar-light navbar-sticky header-static">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <div class="w-100  d-flex">

                    <!-- Responsive navbar toggler -->
                    <button class="navbar-toggler me-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="text-muted h6 ps-3">Menu</span>
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- Main navbar START -->
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav navbar-nav-scroll navbar-lh-sm">

                            <!-- Nav item 1 Demos -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle active" href="#" id="homeMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
                                <ul class="dropdown-menu" aria-labelledby="homeMenu">
                                    <li> <a class="dropdown-item" href="index.html">Home default</a></li>
                                    <li> <a class="dropdown-item" href="index-2.html">Magazine classic</a></li>
                                    <li> <a class="dropdown-item active" href="index-3.html">Magazine</a></li>
                                    <li> <a class="dropdown-item" href="index-4.html">Home cards</a></li>
                                    <li> <a class="dropdown-item" href="index-5.html">Blog classic</a></li>
                                </ul>
                            </li>

                            <!-- Nav item 2 Pages -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="pagesMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                <ul class="dropdown-menu" aria-labelledby="pagesMenu">
                                    <li> <a class="dropdown-item" href="about-us.html">About</a></li>
                                    <li> <a class="dropdown-item" href="contact-us.html">Contact</a></li>
                                    <!-- Dropdown submenu -->
                                    <li class="dropdown-submenu dropend">
                                        <a class="dropdown-item dropdown-toggle" href="#">Other Archives</a>
                                        <ul class="dropdown-menu dropdown-menu-start" data-bs-popper="none">
                                            <li> <a class="dropdown-item" href="author.html">Author Page</a> </li>
                                            <li> <a class="dropdown-item" href="categories.html">Category page 1</a> </li>
                                            <li> <a class="dropdown-item" href="categories-2.html">Category page 2</a> </li>
                                            <li> <a class="dropdown-item" href="tag.html"># tag</a> </li>
                                            <li> <a class="dropdown-item" href="search-result.html">Search result</a> </li>
                                        </ul>
                                    </li>
                                    <li> <a class="dropdown-item" href="404.html">Error 404</a></li>
                                    <li> <a class="dropdown-item" href="signin.html">signin</a></li>
                                    <li> <a class="dropdown-item" href="signup.html">signup</a></li>
                                    <!-- Dropdown submenu levels -->
                                    <li class="dropdown-divider"></li>
                                    <li class="dropdown-submenu dropend">
                                        <a class="dropdown-item dropdown-toggle" href="#">Dropdown levels</a>
                                        <ul class="dropdown-menu dropdown-menu-start" data-bs-popper="none">
                                            <!-- dropdown submenu open right -->
                                            <li class="dropdown-submenu dropend">
                                                <a class="dropdown-item dropdown-toggle" href="#">Dropdown (end)</a>
                                                <ul class="dropdown-menu" data-bs-popper="none">
                                                    <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                                    <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                                </ul>
                                            </li>
                                            <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                            <!-- dropdown submenu open left -->
                                            <li class="dropdown-submenu dropstart">
                                                <a class="dropdown-item dropdown-toggle" href="#">Dropdown (start)</a>
                                                <ul class="dropdown-menu dropdown-menu-end" data-bs-popper="none">
                                                    <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                                    <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                                </ul>
                                            </li>
                                            <li> <a class="dropdown-item" href="#">Dropdown item</a> </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-divider"></li>
                                    <li>
                                        <a class="dropdown-item" href="https://support.webestica.com/" target="_blank">
                                            <i class="text-warning fa-fw bi bi-life-preserver me-2"></i>Support
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="docs/index.html" target="_blank">
                                            <i class="text-danger fa-fw bi bi-card-text me-2"></i>Documentation
                                        </a>
                                    </li>
                                    <li class="dropdown-divider"></li>
                                    <li>
                                        <a class="dropdown-item" href="https://blogzine.webestica.com/rtl" target="_blank">
                                            <i class="text-info fa-fw bi bi-toggle-off me-2"></i>RTL demo
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="https://themes.getbootstrap.com/store/webestica/" target="_blank">
                                            <i class="text-success fa-fw bi bi-cloud-download-fill me-2"></i>Buy blogzine!
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Nav item 3 Post -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="postMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Post</a>
                                <ul class="dropdown-menu" aria-labelledby="postMenu">
                                    <!-- dropdown submenu -->
                                    <li class="dropdown-submenu dropend">
                                        <a class="dropdown-item dropdown-toggle" href="#">Post grid</a>
                                        <ul class="dropdown-menu dropdown-menu-start" data-bs-popper="none">
                                            <li> <a class="dropdown-item" href="post-grid.html">Post grid</a> </li>
                                            <li> <a class="dropdown-item" href="post-grid-4-col.html">Post grid 4 col</a> </li>
                                            <li> <a class="dropdown-item" href="post-grid-masonry.html">Post grid masonry</a> </li>
                                            <li> <a class="dropdown-item" href="post-grid-masonry-filter.html">Post grid masonry filter</a> </li>
                                            <li> <a class="dropdown-item" href="post-large-and-grid.html">Post mixed large than grid</a> </li>
                                        </ul>
                                    </li>
                                    <li> <a class="dropdown-item" href="post-list.html">Post list</a> </li>
                                    <li> <a class="dropdown-item" href="post-list-2.html">Post list 2</a> </li>
                                    <li> <a class="dropdown-item" href="post-cards.html">Post card</a> </li>
                                    <li> <a class="dropdown-item" href="post-overlay.html">Post overlay</a> </li>
                                    <li class="dropdown-divider"></li>
                                    <li> <a class="dropdown-item" href="post-single.html">Post single magazine</a> </li>
                                    <li> <a class="dropdown-item" href="post-single-2.html">Post single classic</a> </li>
                                    <li> <a class="dropdown-item" href="post-single-3.html">Post single minimal</a> </li>
                                    <li> <a class="dropdown-item" href="post-single-4.html">Post single card</a> </li>
                                    <li> <a class="dropdown-item" href="post-single-5.html">Post single review</a> </li>
                                    <li> <a class="dropdown-item" href="post-single-6.html">Post single video</a> </li>
                                    <li class="dropdown-divider"></li>
                                    <li> <a class="dropdown-item" href="pagination-styles.html">Pagination styles</a> </li>
                                </ul>
                            </li>

                            <!-- Nav item 4 Mega menu -->
                            <li class="nav-item dropdown dropdown-fullwidth">
                                <a class="nav-link dropdown-toggle" href="#" id="megaMenu" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Lifestyle</a>
                                <div class="dropdown-menu" aria-labelledby="megaMenu">
                                    <div class="container">
                                        <div class="row g-4 p-3 flex-fill">
                                            <!-- Card item START -->
                                            <div class="col-sm-6 col-lg-3">
                                                <div class="card bg-transparent">
                                                    <!-- Card img -->
                                                    <img class="card-img rounded" src="theme/images/blog/16by9/small/01.jpg" alt="Card image">
                                                    <div class="card-body px-0 pt-3">
                                                        <h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">7 common mistakes everyone makes while traveling</a></h6>
                                                        <!-- Card info -->
                                                        <ul class="nav nav-divider align-items-center text-uppercase small mt-2">
                                                            <li class="nav-item">
                                                                <a href="#" class="text-reset btn-link">Joan Wallace</a>
                                                            </li>
                                                            <li class="nav-item">Feb 18, 2021</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Card item END -->
                                            <!-- Card item START -->
                                            <div class="col-sm-6 col-lg-3">
                                                <div class="card bg-transparent">
                                                    <!-- Card img -->
                                                    <img class="card-img rounded" src="theme/images/blog/16by9/small/02.jpg" alt="Card image">
                                                    <div class="card-body px-0 pt-3">
                                                        <h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">12 worst types of business accounts you follow on Twitter</a></h6>
                                                        <!-- Card info -->
                                                        <ul class="nav nav-divider align-items-center text-uppercase small mt-2">
                                                            <li class="nav-item">
                                                                <a href="#" class="text-reset btn-link">Lori Stevens</a>
                                                            </li>
                                                            <li class="nav-item">Jun 03, 2021</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Card item END -->
                                            <!-- Card item START -->
                                            <div class="col-sm-6 col-lg-3">
                                                <div class="card bg-transparent">
                                                    <!-- Card img -->
                                                    <img class="card-img rounded" src="theme/images/blog/16by9/small/03.jpg" alt="Card image">
                                                    <div class="card-body px-0 pt-3">
                                                        <h6 class="card-title mb-0"><a href="#" class="btn-link text-reset fw-bold">Skills that you can learn from business</a></h6>
                                                        <!-- Card info -->
                                                        <ul class="nav nav-divider align-items-center text-uppercase small mt-2">
                                                            <li class="nav-item">
                                                                <a href="#" class="text-reset btn-link">Judy Nguyen</a>
                                                            </li>
                                                            <li class="nav-item">Sep 07, 2021</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Card item END -->
                                            <!-- Card item START -->
                                            <div class="col-sm-6 col-lg-3">
                                                <div class="bg-primary-soft p-4 text-center h-100 w-100 rounded">
                                                    <span>The Blogzine</span>
                                                    <h3>Premium Membership</h3>
                                                    <p>Become a Member Today!</p>
                                                    <a href="#" class="btn btn-warning">View pricing plans</a>
                                                </div>
                                            </div>
                                            <!-- Card item END -->
                                        </div> <!-- Row END -->
                                        <!-- Trending tags -->
                                        <div class="row px-3">
                                            <div class="col-12">
                                                <ul class="list-inline mt-3">
                                                    <li class="list-inline-item">Trending tags:</li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-primary-soft">Travel</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-warning-soft">Business</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-success-soft">Tech</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-danger-soft">Gadgets</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-info-soft">Lifestyle</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-primary-soft">Vaccine</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-success-soft">Sports</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-danger-soft">Covid-19</a></li>
                                                    <li class="list-inline-item"><a href="#" class="btn btn-sm btn-info-soft">Politics</a></li>
                                                </ul>
                                            </div>
                                        </div> <!-- Row END -->
                                    </div>
                                </div>
                            </li>

                            <!-- Nav item 5 link-->
                            <li class="nav-item">	<a class="nav-link" href="../docs/alerts.html">Components</a></li>
                        </ul>
                    </div>
                    <!-- Main navbar END -->

                    <!-- Nav right START -->
                    <div class="nav flex-nowrap align-items-center me-2">
                        <!-- Dark mode switch -->
                        <div class="nav-item">
                            <div class="modeswitch" id="darkModeSwitch">
                                <div class="switch"></div>
                            </div>
                        </div>
                        <!-- Nav bookmark -->
                        <div class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="bi bi-bookmark-heart fs-4"></i>
                            </a>
                        </div>
                        <!-- Nav user -->
                        <div class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="bi bi-person-square fs-4"></i>
                            </a>
                        </div>
                        <!-- Nav Search -->
                        <div class="nav-item dropdown nav-search dropdown-toggle-icon-none">
                            <a class="nav-link text-uppercase dropdown-toggle" role="button" href="#" id="navSearch" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-search fs-4"> </i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end shadow rounded p-2" aria-labelledby="navSearch">
                                <form class="input-group">
                                    <input class="form-control border-success" type="search" placeholder="Search" aria-label="Search">
                                    <button class="btn btn-success m-0" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <!-- Offcanvas menu toggler -->
                        <div class="nav-item">
                            <a class="nav-link pe-0" data-bs-toggle="offcanvas" href="#offcanvasMenu" role="button" aria-controls="offcanvasMenu">
                                <i class="bi bi-text-right rtl-flip fs-2" data-bs-target="#offcanvasMenu"> </i>
                            </a>
                        </div>
                    </div>
                    <!-- Nav right END -->
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar END -->
</header>
<!-- =======================
Header END -->

<!-- **************** MAIN CONTENT START **************** -->
<main>

   @yield('content')

</main>
<!-- **************** MAIN CONTENT END **************** -->

<!-- =======================
Footer START -->
<footer class="pt-5">
    <div class="container">
        <!-- About and Newsletter START -->
        <div class="row pt-3 pb-4">
            <div class="col-md-3">
                <img src="{{ asset('theme/images/logo-gulfthis.svg') }}" alt="footer logo">
            </div>
            <div class="col-md-5">
                <p >The next-generation blog, news, and magazine theme for you to start sharing your stories today! This Bootstrap 5 based theme is ideal for all types of sites that deliver the news.</p>
            </div>
            <div class="col-md-4">
                <!-- Form -->
                <form class="row row-cols-lg-auto g-2 align-items-center justify-content-end">
                    <div class="col-12">
                        <input type="email" class="form-control" placeholder="Enter your email address">
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary m-0">Subscribe</button>
                    </div>
                    <div class="form-text mt-2">By subscribing you agree to our
                        <a href="#" class="text-decoration-underline text-reset">Privacy Policy</a>
                    </div>
                </form>
            </div>
        </div>
        <!-- About and Newsletter END -->
        <div class="row ">

            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>About Gulfthis</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/company/about-us">Who we are</a></li>
                    <li><a href="https://gulfthis.com/company/investor-relations">Investor Relations</a></li>
                    <li><a href="https://gulfthis.com/company/editorial">Editorial Policy</a></li>
                    <li><a href="https://gulfthis.com/company/media-kit">Media kit</a></li>
                    <li><a href="https://gulfthis.com/company/careers">Careers</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Tools</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/public-holidays-in-bahrain">Public Holidays in Bahrain</a></li>
                    <li><a href="https://gulfthis.com/covid-19-updates-bahrain">Live COVID19 Tracker</a></li>
                    <li><a href="https://gulfthis.com/bahrain-indemnity-calculator">Bahrain Indemnity Calculator</a></li>


                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>For Businesses</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/businesses/add-listing">List your Business</a></li>
                    <li><a href="https://gulfthis.com/businesses/claim-listing">Claim your Listings</a></li>
                    <li><a href="https://gulfthis.com/dealer-solutions">Business Solutions</a></li>
                    <li><a href="https://gulfthis.com/advertise-with-us">Advertise with us</a></li>

                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Our mobile App</h5>
                <p>Download our App and get the latest Breaking News Alerts and latest headlines and daily articles near you.</p>
                <div class="row g-2">
                    <div class="col">
                        <a href="#"><img class="w-100" src="theme/images/app-store.svg" alt="app-store"></a>
                    </div>
                    <div class="col">
                        <a href="#"><img class="w-100" src="theme/images/google-play.svg" alt="google-play"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row-->
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p class="copy-right">By continuing past this page, you agree to our <a href="https://gulfthis.com/terms-of-use">Terms of use</a>,<a href="https://gulfthis.com/cookie-policy"> Cookie Policy</a>, <a href="https://gulfthis.com/editorial-policy"> Editorial Policy</a>,<a href="https://gulfthis.com/privacy-policy"> Privacy Policy</a> and <a href="https://gulfthis.com/content-policy"> Content Policies</a>. All trademarks are properties
                    of their respective owners. © 2020. <a href="https://gulfthis.com/">Gulfthis - Bahrain's No.1
                        Local Search Engine</a> . All rights reserved.</p>
            </div>
        </div>
        <hr>
        <div class="footer-info">
            <div class="row">
                <div class="col-12">

                    <h4>Have news to share?</h4>
                    <p>Email us at <a href="mail:editorial@gulfthis.com">editorial@gulfthis.com</a> , or send us
                        your company press releases to <a href="mail:pressrelease@gulfthis.com">pressrelease@gulfthis.com</a>
                    </p>
                    <h4>Gulfthis - Bahrain's No.1 Local Search Engine </h4>
                    <p class="copy-right">Gulfthis.com is Bahrain's No. 1 Local Search engine that provides local
                        search related services to users across Bahrain through multiple platforms such as website,
                        mobile website, Apps (Android, iOS). GulfThis.com is a one of its kind Hyper Local portal,
                        which lets you know the whole spectrum of what’s happening in the Kingdom of Bahrain.</p>
                    <h4>Some of our services that will prove useful to you on a day-to-day basis are:</h4>
                    <div class="row">
                        <div class="col-lg-4">
                            <h6>Gulfthis - Shorts News</h6>
                            <p class="copy-right">Gulfthis Instant news is a news app that selects latest and best
                                news from multiple national and international sources and summarises them to present
                                in a short &amp; image format,
                                personalized for you.</p>

                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Automotive Guide in Bahrain </h6>
                            <p class="copy-right">A platform where car buyers and owners can research, buy, sell and
                                come together to discuss and talk about their cars.</p>
                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Business </h6>
                            <p class="copy-right">Search for anything in Bahrain, anywhere you are with GulfThis
                                Business app.GulfThis Business app now gives you phone numbers, maps, diections and
                                more about every business in Bahrain, all on the go.</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</footer>
<!-- =======================
Footer END -->

<!-- =======================
Cookies alert START -->
<div class="alert alert-dismissible fade show bg-dark text-white position-fixed start-0 bottom-0 z-index-99 shadow p-4 ms-3 mb-3 col-9 col-md-4 col-lg-3 col-xl-2" role="alert">
    This website stores cookies on your computer. To find out more about the cookies we use, see our <a class="text-white" href="#"> Privacy Policy</a>
    <div class="mt-4">
        <button type="button" class="btn btn-success-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">Accept</span>
        </button>
        <button type="button" class="btn btn-danger-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">Decline</span>
        </button>
    </div>
    <div class="position-absolute end-0 top-0 mt-n3 me-n3"><img class="w-100" src="theme/images/cookie.svg" alt="cookie"></div>
</div>
<!-- =======================
Cookies alert END -->

<!-- Back to top -->
<div class="back-top"><i class="bi bi-arrow-up-short"></i></div>

<!-- =======================
JS libraries, plugins and custom scripts -->

<!-- Bootstrap JS -->
<script src="{{ asset('theme/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- Vendors -->
<script src="{{ asset('theme/vendor/tiny-slider/tiny-slider.js') }} "></script>
<script src="{{ asset('theme/vendor/sticky-js/sticky.min.js') }}"></script>
<script src="{{ asset('theme/vendor/glightbox/js/glightbox.js') }}"></script>

<!-- Template Functions -->
<script src="{{ asset('theme/js/functions.js') }}"></script>

</body>
</html>