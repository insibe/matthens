<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" id="viewport"content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=yes, shrink-to-fit=no, minimal-ui, viewport-fit=cover">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="https://gulfthis.com/" itemprop="url">
    <meta name="google-site-verification" content="UOlx-Os2VCgYw_4GazZB1CeHRYomE-tLDFJVY2DSMPw"/>
    <title itemprop="name">@yield('title','Gulfthis | Bahrain  No.1 Local Search Engine & business listings service')</title>
    <meta name="keywords" content="@yield('meta_keywords','Bahrain, Bahrain News')">
    <meta name="description"
          content="@yield('meta_description','Gulfthis, Bahrain local search engine, Gulfthis curates Social content, News, Videos & more from Top Publishers on all Trending Topics.')">
    <link rel="canonical" href="{{url()->current()}}"/>

    <meta property="og:url" content="@yield('ogUrl','https://www.gulfthis.com/finance/gold-rate-in-bahrain/') "/>
    <meta property="og:title" content=" @yield('ogTitle','Gold Rate in Kingdom of Bahrain - GulfThis')"/>
    <meta property="og:description" content="@yield('ogDescription','Check latest gold rate in bahrain in indian rupees and bahraini dinar per gram, tola, sovereign, ounce and kilogram. 24k, 22k, 21k, 18k gold rate in kingdom of bahrain, gold bars, gold biscuits and gold coins prices in bahrain today.') "/>
    <meta property="og:image" content="@yield('ogImage','https://www.goldenchennai.com/finance/images/gold/gold-rate-in-bahrain.gif')"/>

    {{--    <meta property='article:published_time' content='2015-01-31T20:30:11-02:00' />--}}
    {{--    <meta property='article:section' content='news' />--}}

    {{--    <meta property="og:description"content="description..." />--}}
    {{--    <meta property="og:title"content="Title" />--}}
    {{--    <meta property="og:url"content="http://current.url.com" />--}}
    {{--    <meta property="og:type"content="article" />--}}
    {{--    <meta property="og:locale"content="pt-br" />--}}
    {{--    <meta property="og:locale:alternate"content="pt-pt" />--}}
    {{--    <meta property="og:locale:alternate"content="en-us" />--}}
    {{--    <meta property="og:site_name"content="name" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img1.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img2.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img3.jpg" />--}}
    {{--    <meta property="og:image:url"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image:size"content="300" />--}}

    {{--    <meta name="twitter:card"content="summary" />--}}
    {{--    <meta name="twitter:title"content="Title" />--}}
    {{--    <meta name="twitter:site"content="@LuizVinicius73" />--}}

    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <!-- OR with multi -->--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebPage","name":"Title - Over 9000 Thousand!"}</script>--}}


    <meta name="format-detection" content="telephone=no">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="x-rim-auto-match" content="none">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <meta name="msapplication-TileImage" content="images/app-icons/icon-152x152.png">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Progressive Web Application">
    <meta name="application-name" content="gulfthis">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }} " type="image/x-icon"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}/">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/android-chrome-192x192.png') }}" sizes="192x192">
    <meta name="msapplication-square70x70logo" content="{{ asset('assets/images/smalltile.png') }}"/>
    <meta name="msapplication-square150x150logo" content="{{ asset('assets/images/mediumtile.png') }}"/>
    <meta name="msapplication-wide310x150logo" content="{{ asset('assets/images/widetile.png') }}"/>
    <meta name="msapplication-square310x310logo" content="{{ asset('assets/images/largetile.png') }}"/>

    <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- BASE CSS -->
    {{--    <link href="{{ asset('assets/css/bootstrap.min.css') }} " rel="stylesheet">--}}
    <link href="{{ asset('assets/css/theme.bundle.css') }} " rel="stylesheet">
    <link href="{{ asset('assets/css/libs.bundle.css') }} " rel="stylesheet">

    {{--    <link href="{{ asset('assets/css/style.css') }} " rel="stylesheet">--}}
    {{--    <link href="{{ asset('assets/css/vendors.css') }} " rel="stylesheet">--}}
    <link href="{{ asset('assets/css/custom.css') }} " rel="stylesheet">
    <script data-ad-client="ca-pub-2126121144705193" async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
@yield('custom-scripts-css')
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-DGTPWYP6GN"></script>
    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-DGTPWYP6GN');
    </script>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org/",
          "@type": "WebSite",
          "name": "Gulfthis - Bahrain local search engine",
          "url": "https://gulfthis.com/",
          "@id":"https://www.gulfthis.com/#website",
          "inLanguage":"en-US",
          "description":"Gulfthis, Bahrain local search engine, Gulfthis curates Social content, News, Videos &amp; more from Top Publishers on all Trending Topics",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "https://gulfthis.com/search?search={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }

    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60f269389753177b"></script>
</head>

<body>

<div>
    <header class="p-3 ">

        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                    <img src="{{ asset('assets/images/logo-gulfthis.svg') }}">
                </a>

                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                    <li><a href="{{ url('/list-companies-in-bahrain') }}" class="nav-link px-2 ">Companies</a></li>
                    <li><a href="{{ url('/articles') }}" class="nav-link px-2 ">News</a></li>
                    <li><a href="{{ url('finance/gold-rate-in-bahrain') }}" class="nav-link px-2 ">Gold Rates</a></li>
                    <li><a href="{{ url('/banking-in-bahrain') }}" class="nav-link px-2 ">Banking</a></li>
                    <li><a href="{{ url('/automotive') }}" class="nav-link px-2 ">Automotive</a></li>
                    <li><a href="{{ url('/guides') }}" class="nav-link px-2 ">Guides</a></li>
                </ul>


                <div class="text-end">

                    <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                        <li><a href="{{ url('/add-your-business') }}" class="btn btn-sm btn-primary me-2">Add Your
                                Business</a></li>

                        @if (Auth::check())
                            <li><a href="{{ url('/account') }}"
                                   class="btn btn-sm btn-secondary me-2">{{ Auth::user()->name }}</a></li>
                            <li><a href="{{ url('/dashboard') }}" class="btn btn-sm btn-dark me-2">Dashboard</a></li>

                            <li><a href="{{ url('/dashboard/posts/create') }}" class="btn btn-sm btn-dark me-2">New
                                    Post</a></li>


                        @else

                            <li><a href="{{ url('/login') }}" class="btn btn-secondary me-2">{{ __('Login') }}</a></li>
                        @endif

                    </ul>


                    <!-- Authentication Links -->

                    {{--                        <div class="dropdown">--}}
                    {{--                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">--}}

                    {{--                            </button>--}}
                    {{--                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">--}}
                    {{--                                <li><a class="dropdown-item" href="{{ route('logout') }}"--}}
                    {{--                                       onclick="event.preventDefault();--}}
                    {{--                                                     document.getElementById('logout-form').submit();">--}}
                    {{--                                        {{ __('Logout') }}--}}
                    {{--                                    </a>--}}
                    {{--                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
                    {{--                                        @csrf--}}
                    {{--                                    </form>--}}
                    {{--                                </li>--}}

                    {{--                            </ul>--}}
                    {{--                        </div>--}}

                    {{--                        @endguest--}}

                </div>
            </div>
        </div>
    </header>
    <!-- /header -->

    <main>

        @yield('content')
    </main>
    <!-- /main -->

    <footer>

        <div class="container">

            <div class="row ">

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>About Gulfthis</h5>
                    <ul class="links">
                        <li><a href="{{ url('company/about-us') }}">Who we are</a></li>
                        <li><a href="{{ url('company/investor-relations') }}">Investor Relations</a></li>
                        <li><a href="{{ url('company/editorial') }}">Editorial Policy</a></li>
                        <li><a href="{{ url('company/media-kit')}}">Media kit</a></li>
                        <li><a href="{{ url('company/careers') }}">Careers</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>Tools</h5>
                    <ul class="links">
                        <li><a href="{{ url('/public-holidays-in-bahrain') }}">Public Holidays in Bahrain</a></li>
                        <li><a href="{{ url('/covid-19-updates-bahrain') }}">Live COVID19 Tracker</a></li>
                        <li><a href="{{ url('/bahrain-indemnity-calculator') }}">Bahrain Indemnity Calculator</a></li>


                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>For Businesses</h5>
                    <ul class="links">
                        <li><a href="{{ url('businesses/add-listing') }}">List your Business</a></li>
                        <li><a href="{{ url('businesses/claim-listing') }}">Claim your Listings</a></li>
                        <li><a href="{{ url('/dealer-solutions') }}">Business Solutions</a></li>
                        <li><a href="{{ url('/advertise-with-us') }}">Advertise with us</a></li>

                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <h5>Others </h5>
                    <ul class="links">
                        <li><a href="#0">Disclaimer</a></li>
                        <li><a href="#0">Contact Us</a></li>
                    </ul>
                    <ul class="list-unstyled list-inline list-social mb-6 mb-md-0">
                        <li class="list-inline-item list-social-item me-3">
                            <a href="#!" class="text-decoration-none">
                                <img src="{{ asset('img/icons/social/instagram.svg') }}" class="list-social-icon"
                                     alt="...">
                            </a>
                        </li>
                        <li class="list-inline-item list-social-item me-3">
                            <a href="#!" class="text-decoration-none">
                                <img src="./assets/img/icons/social/facebook.svg" class="list-social-icon" alt="...">
                            </a>
                        </li>
                        <li class="list-inline-item list-social-item me-3">
                            <a href="#!" class="text-decoration-none">
                                <img src="./assets/img/icons/social/twitter.svg" class="list-social-icon" alt="...">
                            </a>
                        </li>
                        <li class="list-inline-item list-social-item">
                            <a href="#!" class="text-decoration-none">
                                <img src="./assets/img/icons/social/pinterest.svg" class="list-social-icon" alt="...">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /row-->
            <hr>
            <div class="row">
                <div class="col-lg-12">
                    <p class="copy-right">By continuing past this page, you agree to our <a
                                href="{{ url('terms-of-use') }}">Terms of use</a>,<a
                                href="{{ url('cookie-policy') }}"> Cookie Policy</a>, <a
                                href="{{ url('editorial-policy') }}"> Editorial Policy</a>,<a
                                href="{{ url('privacy-policy') }}"> Privacy Policy</a> and <a
                                href="{{ url('content-policy') }}"> Content Policies</a>. All trademarks are properties
                        of their respective owners. © 2020. <a href="https://gulfthis.com/">Gulfthis - Bahrain's No.1
                            Local Search Engine</a> . All rights reserved.</p>
                </div>
            </div>
            <hr>
            <div class="footer-info">
                <div class="row">
                    <div class="col-12">

                        <h4>Have news to share?</h4>
                        <p>Email us at <a href="mail:editorial@gulfthis.com">editorial@gulfthis.com</a> , or send us
                            your company press releases to <a href="mail:pressrelease@gulfthis.com">pressrelease@gulfthis.com</a>
                        </p>
                        <h4>Gulfthis - Bahrain's No.1 Local Search Engine </h4>
                        <p class="copy-right">Gulfthis.com is Bahrain's No. 1 Local Search engine that provides local
                            search related services to users across Bahrain through multiple platforms such as website,
                            mobile website, Apps (Android, iOS). GulfThis.com is a one of its kind Hyper Local portal,
                            which lets you know the whole spectrum of what’s happening in the Kingdom of Bahrain.</p>
                        <h4>Some of our services that will prove useful to you on a day-to-day basis are:</h4>
                        <div class="row">
                            <div class="col-lg-4">
                                <h6>Gulfthis - Shorts News</h6>
                                <p class="copy-right">Gulfthis Instant news is a news app that selects latest and best
                                    news from multiple national and international sources and summarises them to present
                                    in a short & image format,
                                    personalized for you.</p>

                            </div>
                            <div class="col-lg-4">
                                <h6>Gulfthis - Automotive Guide in Bahrain </h6>
                                <p class="copy-right">A platform where car buyers and owners can research, buy, sell and
                                    come together to discuss and talk about their cars.</p>
                            </div>
                            <div class="col-lg-4">
                                <h6>Gulfthis - Business </h6>
                                <p class="copy-right">Search for anything in Bahrain, anywhere you are with GulfThis
                                    Business app.GulfThis Business app now gives you phone numbers, maps, diections and
                                    more about every business in Bahrain, all on the go.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </footer>

    <!--/footer-->
</div>
<!-- page -->

<!-- /Sign In Popup -->

<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script type="script" src="{{ asset('assets/js/bootstrap.bundle.js') }}"></script>
<script type="script" src="{{ asset('assets/js/common_scripts.js') }}"></script>
{{--<script src="{{ asset('assets/js/functions.js') }}"></script>--}}
{{--<script src="{{ asset('assets/validate.js') }}"></script>--}}

@yield('custom-scripts-js')


</body>
</html>
