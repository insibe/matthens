<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />

    <!-- Favicon -->
    <link rel="shortcut icon" href="./assets/favicon/favicon.ico" type="image/x-icon"/>
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{ asset('db/css/libs.bundle.css') }}" />
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('db/css/theme.bundle.css') }}" />
    <!-- Title -->
    <title> Dashboard </title>
</head>
<body>

<!-- NAVIGATION -->
<nav class="navbar navbar-vertical fixed-start navbar-expand-md navbar-light" id="sidebar">
    <div class="container-fluid">

        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <!-- User (md) -->
        <div class="navbar-user d-none d-md-flex" id="sidebarUser">

            <!-- Icon -->
            <a class="navbar-user-link" data-bs-toggle="offcanvas" href="#sidebarOffcanvasActivity" aria-controls="sidebarOffcanvasActivity">
            <span class="icon">
              <i class="fe fe-bell"></i>
            </span>
            </a>

            <!-- Dropup -->
            <div class="dropdown">

                <!-- Toggle -->
                <a href="#" id="sidebarIconCopy" class="dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="avatar avatar-xs">
                        <img src="{{ asset('db/img/avatars/profiles/avatar-1.jpg') }} " class="avatar-img rounded-circle" alt="Profile picture">
                    </div>
                </a>

                <!-- Menu -->
                <div class="dropdown-menu" aria-labelledby="sidebarIconCopy">
                    <a href="./profile-posts.html" class="dropdown-item">Profile</a>
                    <a href="./account-general.html" class="dropdown-item">Account</a>
                    <hr class="dropdown-divider">
                    <a href="./sign-in.html" class="dropdown-item">Logout &nbsp;&nbsp; <span class="fe fe-log-out"></span></a>
                </div>

            </div>

            <!-- Icon -->
            <a class="navbar-user-link" data-bs-toggle="offcanvas" href="#sidebarOffcanvasSearch" aria-controls="sidebarOffcanvasSearch">
            <span class="icon">
              <i class="fe fe-search"></i>
            </span>
            </a>

        </div>

        <!-- User (xs) -->
        <div class="navbar-user d-md-none">

            <!-- Dropdown -->
            <div class="dropdown">

                <!-- Toggle -->
                <a href="#" id="sidebarIcon" class="dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="avatar avatar-sm">
                        <img src="./assets/img/avatars/profiles/avatar-1.jpg" class="avatar-img rounded-circle" alt="...">
                    </div>
                </a>

                <!-- Menu -->
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="sidebarIcon">
                    <a href="./profile-posts.html" class="dropdown-item">Profile</a>
                    <a href="./account-general.html" class="dropdown-item">Account</a>
                    <hr class="dropdown-divider">
                    <a href="./sign-in.html" class="dropdown-item">Logout &nbsp;&nbsp; <span class="fe fe-log-out"></span></a>
                </div>

            </div>

        </div>

        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidebarCollapse">

            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge input-group-reverse">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-text">
                        <span class="fe fe-search"></span>
                    </div>
                </div>
            </form>






            <!-- Divider -->
            <hr class="navbar-divider my-3">

            <!-- Heading -->
            <h6 class="navbar-heading">

            </h6>

            <!-- Navigation -->
            <ul class="navbar-nav mb-md-4">
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarData" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarData">
                        <i class="fe fe-database"></i> Posts
                    </a>
                    <div class="collapse " id="sidebarData">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ url('dashboard/posts') }}" class="nav-link ">
                                    All Posts
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('dashboard/posts/create') }}" class="nav-link ">
                                   Add New
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('dashboard/categories?type=posts') }}" class="nav-link ">
                                    Categories
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('dashboard/sources') }}" class="nav-link ">
                                    Sources
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href=" {{ url('dashboard/tags?type=posts') }}" class="nav-link ">
                                   Tags
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarSource" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSource">
                        <i class="fe fe-database"></i>Feeds
                    </a>
                    <div class="collapse " id="sidebarSource">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ url('dashboard/posts') }}" class="nav-link ">
                                    All Feeds
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('dashboard/posts/create') }}" class="nav-link ">
                                    Add New Feed
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('dashboard/categories?type=posts') }}" class="nav-link ">
                                    Categories
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarInsights" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarInsights">
                        <i class="fe fe-database"></i> Business
                    </a>
                    <div class="collapse " id="sidebarInsights">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ url('dashboard/businesses') }}" class="nav-link ">
                                    All Businesses
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="./competitor-insights.html" class="nav-link ">
                                   Add New
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="./market-insights.html" class="nav-link ">
                                   Categories
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <hr class="navbar-divider my-3">
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarIntelligence" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarIntelligence">
                        <i class="fe fe-cpu"></i> Tools
                    </a>
                    <div class="collapse " id="sidebarIntelligence">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="./sentiment-analysis.html" class="nav-link ">
                                    Import
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="./named-entities-recognition.html" class="nav-link ">
                                   Export
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

                <!-- Divider -->


                <li class="nav-item">
                    <a href="./store.html" class="nav-link ">
                        <i class="fe fe-shopping-cart"></i>Settings
                    </a>
                </li>
            </ul>

            <!-- Push content down -->
            <div class="mt-auto"></div>


            <!-- Navigation -->
{{--            <ul class="navbar-nav">--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="#sidebarHelp" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarHelp">--}}
{{--                        <i class="fe fe-help-circle"></i> Help Center--}}
{{--                    </a>--}}
{{--                    <div class="collapse " id="sidebarHelp">--}}
{{--                        <ul class="nav nav-sm flex-column">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./getting-started.html" class="nav-link ">--}}
{{--                                    Getting started--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./product-tour.html" class="nav-link ">--}}
{{--                                    Product tour--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./knowledge-base.html" class="nav-link ">--}}
{{--                                    Knowledge Base--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="./on-demand-services.html" class="nav-link ">--}}
{{--                                    On demand services--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link " href="./account-general.html">--}}
{{--                        <i class="fe fe-settings"></i> Settings & Members--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}

        </div> <!-- / .navbar-collapse -->

    </div>
</nav>

<!-- MAIN CONTENT -->
<div class="main-content">
    <div class="container-fluid">
            @yield('content')
    </div>
</div>
<!-- JAVASCRIPT -->

<!-- Map JS -->
<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>

<!-- Vendor JS -->
<script src=" {{ asset('db/js/vendor.bundle.js') }} "></script>

<!-- Theme JS -->
<script src="{{ asset('db/js/theme.bundle.js') }} "></script>
</body>
</html>
