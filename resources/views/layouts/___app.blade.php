<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" id="viewport"content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=yes, shrink-to-fit=no, minimal-ui, viewport-fit=cover">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="https://gulfthis.com/" itemprop="url">
    <meta name="google-site-verification" content="UOlx-Os2VCgYw_4GazZB1CeHRYomE-tLDFJVY2DSMPw"/>
    <title itemprop="name">@yield('title','Gulfthis | Bahrain  No.1 Local Search Engine & business listings service')</title>
    <meta name="keywords" content="@yield('meta_keywords','Bahrain, Bahrain News')">
    <meta name="description"
          content="@yield('meta_description','Gulfthis, Bahrain local search engine, Gulfthis curates Social content, News, Videos & more from Top Publishers on all Trending Topics.')">
    <link rel="canonical" href="{{url()->current()}}"/>

    <meta property="og:url" content="@yield('ogUrl','https://www.gulfthis.com/finance/gold-rate-in-bahrain/') "/>
    <meta property="og:title" content=" @yield('ogTitle','Gold Rate in Kingdom of Bahrain - GulfThis')"/>
    <meta property="og:description" content="@yield('ogDescription','Check latest gold rate in bahrain in indian rupees and bahraini dinar per gram, tola, sovereign, ounce and kilogram. 24k, 22k, 21k, 18k gold rate in kingdom of bahrain, gold bars, gold biscuits and gold coins prices in bahrain today.') "/>
    <meta property="og:image" content="@yield('ogImage','https://www.goldenchennai.com/finance/images/gold/gold-rate-in-bahrain.gif')"/>

    {{--    <meta property='article:published_time' content='2015-01-31T20:30:11-02:00' />--}}
    {{--    <meta property='article:section' content='news' />--}}

    {{--    <meta property="og:description"content="description..." />--}}
    {{--    <meta property="og:title"content="Title" />--}}
    {{--    <meta property="og:url"content="http://current.url.com" />--}}
    {{--    <meta property="og:type"content="article" />--}}
    {{--    <meta property="og:locale"content="pt-br" />--}}
    {{--    <meta property="og:locale:alternate"content="pt-pt" />--}}
    {{--    <meta property="og:locale:alternate"content="en-us" />--}}
    {{--    <meta property="og:site_name"content="name" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img1.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img2.jpg" />--}}
    {{--    <meta property="og:image"content="http://image.url.com/img3.jpg" />--}}
    {{--    <meta property="og:image:url"content="http://image.url.com/cover.jpg" />--}}
    {{--    <meta property="og:image:size"content="300" />--}}

    {{--    <meta name="twitter:card"content="summary" />--}}
    {{--    <meta name="twitter:title"content="Title" />--}}
    {{--    <meta name="twitter:site"content="@LuizVinicius73" />--}}

    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <!-- OR with multi -->--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Article","name":"Title - Over 9000 Thousand!"}</script>--}}
    {{--    <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebPage","name":"Title - Over 9000 Thousand!"}</script>--}}


    <meta name="format-detection" content="telephone=no">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="x-rim-auto-match" content="none">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <meta name="msapplication-TileImage" content="images/app-icons/icon-152x152.png">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Progressive Web Application">
    <meta name="application-name" content="gulfthis">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }} " type="image/x-icon"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('theme/images/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('theme/images/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('theme/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('theme/images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('theme/images/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('theme/images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('theme/images/apple-touch-icon-144x144.png') }}/">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('theme/images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('theme/images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('theme/images/android-chrome-192x192.png') }}" sizes="192x192">
    <meta name="msapplication-square70x70logo" content="{{ asset('theme/images/smalltile.png') }}"/>
    <meta name="msapplication-square150x150logo" content="{{ asset('theme/images/mediumtile.png') }}"/>
    <meta name="msapplication-wide310x150logo" content="{{ asset('theme/images/widetile.png') }}"/>
    <meta name="msapplication-square310x310logo" content="{{ asset('theme/images/largetile.png') }}"/>

    <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/bootstrap-icons/bootstrap-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/tiny-slider/tiny-slider.css') }} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/vendor/glightbox/css/glightbox.css') }}">

    <!-- Theme CSS -->
    <link id="style-switch" rel="stylesheet" type="text/css" href="{{ asset('theme/css/style.css') }} ">

</head>

<body>


<!-- =======================
Header START -->
<header class="navbar-light navbar-sticky header-static">
    <!-- Logo Nav START -->
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <!-- Logo START -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="navbar-brand-item light-mode-item" src="{{ asset('theme/images/gulfthis-logo.svg') }}" alt="logo">
            </a>
            <!-- Logo END -->

            <!-- Responsive navbar toggler -->
            <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="text-body h6 d-none d-sm-inline-block">Menu</span>
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Main navbar START -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav navbar-nav-scroll ms-auto">
                    <!-- Nav item 5 link-->
                    <li class="nav-item">	<a class="nav-link" href="{{ url('/') }}">Read News</a></li>

                </ul>
            </div>
            <!-- Main navbar END -->

            <!-- Nav right START -->
            <div class="nav ms-sm-3 flex-nowrap align-items-center">
                <!-- Dark mode switch -->

                <!-- Nav additional link -->
                <div class="nav-item dropdown dropdown-toggle-icon-none d-none d-sm-block">
                    <a class="nav-link dropdown-toggle" role="button" href="#" id="navAdditionalLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-three-dots fs-4"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end min-w-auto shadow rounded text-end" aria-labelledby="navAdditionalLink">
                        <li><a class="dropdown-item fw-normal" href="#">About</a></li>
                        <li><a class="dropdown-item fw-normal" href="#">Newsletter</a></li>
                        <li><a class="dropdown-item fw-normal" href="#">Author</a></li>
                        <li><a class="dropdown-item fw-normal" href="#">#Tags</a></li>
                        <li><a class="dropdown-item fw-normal" href="#">Contact</a></li>
                        <li><a class="dropdown-item fw-normal" href="#"><span class="badge bg-danger me-2 align-middle">2 Job</span>Careers</a></li>
                    </ul>
                </div>
                <!-- Nav Button -->
                <div class="nav-item d-none d-md-block">
                    <a href="#" class="btn btn-sm btn-danger mb-0 mx-2">Subscribe!</a>
                </div>
                <!-- Nav Search -->
                <div class="nav-item dropdown nav-search dropdown-toggle-icon-none">
                    <a class="nav-link pe-0 dropdown-toggle" role="button" href="#" id="navSearch" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-search fs-4"> </i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end shadow rounded p-2" aria-labelledby="navSearch">
                        <form class="input-group">
                            <input class="form-control border-success" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-success m-0" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Nav right END -->
        </div>
    </nav>
    <!-- Logo Nav END -->
</header>
<!-- =======================
Header END -->

<!-- **************** MAIN CONTENT START **************** -->
<main>

   @yield('content')

</main>
<!-- **************** MAIN CONTENT END **************** -->

<!-- =======================
Footer START -->
<footer class="pt-5">
    <div class="container">
        <!-- About and Newsletter START -->
        <div class="row pt-3 pb-4">
            <div class="col-md-3">
                <img src="{{ asset('theme/images/logo-gulfthis.svg') }}" alt="footer logo">
            </div>
            <div class="col-md-5">
                <p >The next-generation blog, news, and magazine theme for you to start sharing your stories today! This Bootstrap 5 based theme is ideal for all types of sites that deliver the news.</p>
            </div>
            <div class="col-md-4">
                <!-- Form -->
                <form class="row row-cols-lg-auto g-2 align-items-center justify-content-end">
                    <div class="col-12">
                        <input type="email" class="form-control" placeholder="Enter your email address">
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary m-0">Subscribe</button>
                    </div>
                    <div class="form-text mt-2">By subscribing you agree to our
                        <a href="#" class="text-decoration-underline text-reset">Privacy Policy</a>
                    </div>
                </form>
            </div>
        </div>
        <!-- About and Newsletter END -->
        <div class="row ">

            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>About Gulfthis</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/company/about-us">Who we are</a></li>
                    <li><a href="https://gulfthis.com/company/investor-relations">Investor Relations</a></li>
                    <li><a href="https://gulfthis.com/company/editorial">Editorial Policy</a></li>
                    <li><a href="https://gulfthis.com/company/media-kit">Media kit</a></li>
                    <li><a href="https://gulfthis.com/company/careers">Careers</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Tools</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/public-holidays-in-bahrain">Public Holidays in Bahrain</a></li>
                    <li><a href="https://gulfthis.com/covid-19-updates-bahrain">Live COVID19 Tracker</a></li>
                    <li><a href="https://gulfthis.com/bahrain-indemnity-calculator">Bahrain Indemnity Calculator</a></li>


                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>For Businesses</h5>
                <ul class="links">
                    <li><a href="https://gulfthis.com/businesses/add-listing">List your Business</a></li>
                    <li><a href="https://gulfthis.com/businesses/claim-listing">Claim your Listings</a></li>
                    <li><a href="https://gulfthis.com/dealer-solutions">Business Solutions</a></li>
                    <li><a href="https://gulfthis.com/advertise-with-us">Advertise with us</a></li>

                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h5>Our mobile App</h5>
                <p>Download our App and get the latest Breaking News Alerts and latest headlines and daily articles near you.</p>
                <div class="row g-2">
                    <div class="col">
                        <a href="#"><img class="w-100" src="theme/images/app-store.svg" alt="app-store"></a>
                    </div>
                    <div class="col">
                        <a href="#"><img class="w-100" src="theme/images/google-play.svg" alt="google-play"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row-->
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p class="copy-right">By continuing past this page, you agree to our <a href="https://gulfthis.com/terms-of-use">Terms of use</a>,<a href="https://gulfthis.com/cookie-policy"> Cookie Policy</a>, <a href="https://gulfthis.com/editorial-policy"> Editorial Policy</a>,<a href="https://gulfthis.com/privacy-policy"> Privacy Policy</a> and <a href="https://gulfthis.com/content-policy"> Content Policies</a>. All trademarks are properties
                    of their respective owners. © 2020. <a href="https://gulfthis.com/">Gulfthis - Bahrain's No.1
                        Local Search Engine</a> . All rights reserved.</p>
            </div>
        </div>
        <hr>
        <div class="footer-info">
            <div class="row">
                <div class="col-12">

                    <h4>Have news to share?</h4>
                    <p>Email us at <a href="mail:editorial@gulfthis.com">editorial@gulfthis.com</a> , or send us
                        your company press releases to <a href="mail:pressrelease@gulfthis.com">pressrelease@gulfthis.com</a>
                    </p>
                    <h4>Gulfthis - Bahrain's No.1 Local Search Engine </h4>
                    <p class="copy-right">Gulfthis.com is Bahrain's No. 1 Local Search engine that provides local
                        search related services to users across Bahrain through multiple platforms such as website,
                        mobile website, Apps (Android, iOS). GulfThis.com is a one of its kind Hyper Local portal,
                        which lets you know the whole spectrum of what’s happening in the Kingdom of Bahrain.</p>
                    <h4>Some of our services that will prove useful to you on a day-to-day basis are:</h4>
                    <div class="row">
                        <div class="col-lg-4">
                            <h6>Gulfthis - Shorts News</h6>
                            <p class="copy-right">Gulfthis Instant news is a news app that selects latest and best
                                news from multiple national and international sources and summarises them to present
                                in a short &amp; image format,
                                personalized for you.</p>

                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Automotive Guide in Bahrain </h6>
                            <p class="copy-right">A platform where car buyers and owners can research, buy, sell and
                                come together to discuss and talk about their cars.</p>
                        </div>
                        <div class="col-lg-4">
                            <h6>Gulfthis - Business </h6>
                            <p class="copy-right">Search for anything in Bahrain, anywhere you are with GulfThis
                                Business app.GulfThis Business app now gives you phone numbers, maps, diections and
                                more about every business in Bahrain, all on the go.</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</footer>
<!-- =======================
Footer END -->

<!-- =======================
{{--Cookies alert START -->--}}
{{--<div class="alert alert-dismissible fade show bg-dark text-white position-fixed start-0 bottom-0 z-index-99 shadow p-4 ms-3 mb-3 col-9 col-md-4 col-lg-3 col-xl-2" role="alert">--}}
{{--    This website stores cookies on your computer. To find out more about the cookies we use, see our <a class="text-white" href="#"> Privacy Policy</a>--}}
{{--    <div class="mt-4">--}}
{{--        <button type="button" class="btn btn-success-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">--}}
{{--            <span aria-hidden="true">Accept</span>--}}
{{--        </button>--}}
{{--        <button type="button" class="btn btn-danger-soft btn-sm mb-0" data-bs-dismiss="alert" aria-label="Close">--}}
{{--            <span aria-hidden="true">Decline</span>--}}
{{--        </button>--}}
{{--    </div>--}}
{{--    <div class="position-absolute end-0 top-0 mt-n3 me-n3"><img class="w-100" src="theme/images/cookie.svg" alt="cookie"></div>--}}
{{--</div>--}}
{{--<!-- =======================--}}
{{--Cookies alert END -->--}}

<!-- Back to top -->
<div class="back-top"><i class="bi bi-arrow-up-short"></i></div>

<!-- =======================
JS libraries, plugins and custom scripts -->

<!-- Bootstrap JS -->
<script src="{{ asset('theme/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- Vendors -->
<script src="{{ asset('theme/vendor/tiny-slider/tiny-slider.js') }} "></script>
<script src="{{ asset('theme/vendor/sticky-js/sticky.min.js') }}"></script>
<script src="{{ asset('theme/vendor/glightbox/js/glightbox.js') }}"></script>

<!-- Template Functions -->
<script src="{{ asset('theme/js/functions.js') }}"></script>

</body>
</html>