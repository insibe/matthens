@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $posts->count() }} Feeds.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">
                            <li>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-right">
                                        <em class="icon ni ni-search"></em>
                                    </div>
                                    <input type="text" class="form-control" id="default-04" placeholder="Search by name">
                                </div>
                            </li>
                            <li>
                                <div class="drodown">
                                    <a href="#" class="dropdown-toggle dropdown-indicator btn btn-outline-light btn-white" data-toggle="dropdown">Status</a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="link-list-opt no-bdr">
                                            <li><a href="#"><span>Actived</span></a></li>
                                            <li><a href="#"><span>Inactived</span></a></li>
                                            <li><a href="#"><span>Blocked</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/feeds/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Post</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Status</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Title</span></div>
                <div class="nk-tb-col tb-col-lg"><span class="sub-text">Language</span></div>
                <div class="nk-tb-col tb-col-lg"><span class="sub-text">Source</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text"></span></div>
            </div><!-- .nk-tb-item -->
            @if(count($posts ) > 0)
                @foreach($posts as $post)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span>{{ $post->getStatus() }}</span>
                        </div>

                        <div class="nk-tb-col">

                            <a href="{{ url('/dashboard/posts/'.$post->id.'/edit') }}">
                                <span class="tb-lead"> {{ \Str::limit( $post->title ,50) }} </span>
                            </a>
                        </div>
                        <div class="nk-tb-col tb-col-mb">
                            <span>{{ $post->getLanguage() }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-md">
                            <span>{{  $post->feed_id }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            @foreach($post->categories as $category )
                                    <span class="badge badge-secondary">{{ $category->title }}</span>
                            @endforeach
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span></span>
                        </div>

                        <div class="nk-tb-col nk-tb-col-tools">
                            <a href="{{ url('dashboard/posts/'.$post->id.'/fetch') }}" class="btn btn-outline-primary">Fetch</a>
                        </div>
                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $posts->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection