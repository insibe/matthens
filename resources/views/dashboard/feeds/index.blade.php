@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $feeds->count() }} Feeds.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/feeds/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Feed</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">State</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Name</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Type</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Source Language</span></div>
                <div class="nk-tb-col tb-col-lg"><span class="sub-text">Target Language</span></div>
                <div class="nk-tb-col tb-col-lg"><span class="sub-text">Fetch Interval</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Last Fetch</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($feeds ) > 0)
                @foreach($feeds as $feed)
            <div class="nk-tb-item">
                <div class="nk-tb-col tb-col-mb">
                    <span>{{ $feed->getStatus() }}</span>
                </div>

                <div class="nk-tb-col">
                    <a href="{{ url('/dashboard/feeds/'.$feed->id.'/edit') }}">
                        <span class="tb-lead">   {{ $feed->title }} </span>
                    </a>
                </div>
                <div class="nk-tb-col tb-col-mb">
                    <span > {{ $feed->getType() }} </span>
                </div>
                <div class="nk-tb-col tb-col-md">
                    <span>{{ $feed->getSourceLanguage() }}</span>
                </div>
                <div class="nk-tb-col tb-col-lg">
                    <span>{{ $feed->getTargetLanguage() }}</span>
                </div>
                <div class="nk-tb-col tb-col-lg">
                    <span>{{ $feed->interval }}</span>

                </div>
                <div class="nk-tb-col tb-col-lg">
                    <span>{{ $feed->last_fetch }}</span>

                </div>

                <div class="nk-tb-col nk-tb-col-tools">

                    <a href="{{ url('dashboard/feeds/'.$feed->id.'/fetch') }}" class="btn btn-sm btn-outline-primary">Fetch</a>
                </div>
            </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $feeds->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection