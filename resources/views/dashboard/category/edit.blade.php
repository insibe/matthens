@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em
                                class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">


                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-6">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($category, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        {!! Form::text('type',Request::get('type'), ['hidden','required' =>'required']) !!}
                        <div class="form-group">
                            <label class="form-label">Category Language <span>*</span></label>
                            <div class="form-control-wrap">

                                {!! Form::select('locale', [''=>'','en'=>'English', 'ml'=>'Malayalam'] ,null, ['data-parsley-errors-container' => '#locale-errors','data-placeholder' => 'Select Language','class' => ' form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="locale-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="form-label" >Category Title <span>*</span></label>
                                <div class="form-control-wrap">
                                    {!! Form::text('title',null, ['class' => 'form-control', 'placeholder'=>'Enter Category Title..','required' =>'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="phone-no">Category Description</label>
                                <div class="form-control-wrap">
                                    {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Category Description..','required' =>'required']) !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="form-label">Category Parent </label>
                                <div class="form-control-wrap">
                                    {!! Form::select('parent_id',$categoriesList ,null, ['data-parsley-errors-container' => '#parent-category-errors','data-placeholder' => 'Select Parent Category','class' => 'form-control form-select', 'data-search'=>'on']) !!}

                                    <div id="parent-category-errors"></div>
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="form-label" >Category Cover</label>
                            <div class="form-control-wrap">
                                {!! Form::file('cover',null, ['class' => 'cover']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Category Icon</label>
                            <div class="form-control-wrap">
                                {!! Form::file('ocpm',null, ['class' => 'cover']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Category Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Category Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                            <div class="form-group">
{{--                                <form action="{{url('dashboard/feeds/'.$feed->id)}}" method="post">--}}
{{--                                    @method('DELETE')--}}
{{--                                    @csrf--}}
{{--                                    <button type="submit" class="btn btn-lg btn-danger">Delete</button>--}}
{{--                                </form>--}}

                                <button type="submit" class="btn btn-lg btn-primary float-right">Save Category</button>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->





@endsection