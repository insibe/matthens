@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $categories->count() }} Feeds.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/categories/create?type=post') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Category</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Type</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">title</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Description</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Slug</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Items</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($categories ) > 0)
                @foreach($categories as $category)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col tb-col-mb">
                            <span >{{ $category->type }} </span>
                        </div>

                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/category/'.$category->id.'/edit') }}">
                                <span class="tb-lead">   {{ $category->title }} </span>
                            </a>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{ \Str::limit( $category->description ,50) }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">

                                <span class="tb-lead">   {{ $category->slug }} </span>

                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span></span>

                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $categories->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection