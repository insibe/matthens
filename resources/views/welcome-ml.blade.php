@extends('layouts.app-ml')

@section('content')

    <!-- =======================
    Latest news slider START -->
    <section class="py-4 card-grid">
        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- Slider START -->
                    <div class="tiny-slider">
                        <div class="tiny-slider-inner"
                             data-autoplay="true"
                             data-hoverpause="true"
                             data-gutter="24"
                             data-arrow="false"
                             data-dots="false"
                             data-items-md="2"
                             data-items-sm="2"
                             data-items-xs="1"
                             data-items="3" >

                            <!-- Card item START -->
                            <div class="card">
                                <div class="row g-3">
                                    <div class="col-3">
                                        <img class="rounded-3" src="theme/images/blog/1by1/01.jpg" alt="">
                                    </div>
                                    <div class="col-9">
                                        <h5><a href="post-single-5.html" class="btn-link stretched-link text-reset fw-bold">The pros and cons of business agency</a></h5>
                                        <!-- Card info -->
                                        <ul class="nav nav-divider align-items-center small">
                                            <li class="nav-item">
                                                <div class="nav-link position-relative">
                                                    <span>by <a href="#" class="stretched-link text-reset btn-link">Samuel</a></span>
                                                </div>
                                            </li>
                                            <li class="nav-item">Jan 22, 2021</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Card item END -->
                            <!-- Card item START -->
                            <div class="card">
                                <div class="row g-3">
                                    <div class="col-3">
                                        <img class="rounded-3" src="theme/images/blog/1by1/02.jpg" alt="">
                                    </div>
                                    <div class="col-9">
                                        <h5><a href="post-single-5.html" class="btn-link stretched-link text-reset fw-bold">5 reasons why you shouldn't startup</a></h5>
                                        <!-- Card info -->
                                        <ul class="nav nav-divider align-items-center small">
                                            <li class="nav-item">
                                                <div class="nav-link position-relative">
                                                    <span>by <a href="#" class="stretched-link text-reset btn-link">Dennis</a></span>
                                                </div>
                                            </li>
                                            <li class="nav-item">Mar 07, 2021</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Card item END -->
                            <!-- Card item START -->
                            <div class="card">
                                <div class="row g-3">
                                    <div class="col-3">
                                        <img class="rounded-3" src="theme/images/blog/1by1/03.jpg" alt="">
                                    </div>
                                    <div class="col-9">
                                        <h5><a href="post-single-5.html" class="btn-link stretched-link text-reset fw-bold">Five unbelievable facts about money.</a></h5>
                                        <!-- Card info -->
                                        <ul class="nav nav-divider align-items-center small">
                                            <li class="nav-item">
                                                <div class="nav-link position-relative">
                                                    <span>by <a href="#" class="stretched-link text-reset btn-link">Bryan</a></span>
                                                </div>
                                            </li>
                                            <li class="nav-item">Jun 17, 2021</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Card item END -->
                            <!-- Card item START -->
                            <div class="card">
                                <div class="row g-3">
                                    <div class="col-3">
                                        <img class="rounded-3" src="theme/images/blog/1by1/04.jpg" alt="">
                                    </div>
                                    <div class="col-9">
                                        <h5><a href="post-single-5.html" class="btn-link stretched-link text-reset fw-bold">The web: 20 infographics about business</a></h5>
                                        <!-- Card info -->
                                        <ul class="nav nav-divider align-items-center small">
                                            <li class="nav-item">
                                                <div class="nav-link position-relative">
                                                    <span>by <a href="#" class="stretched-link text-reset btn-link">Jacqueline</a></span>
                                                </div>
                                            </li>
                                            <li class="nav-item">Nov 11, 2021</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Card item END -->
                        </div>
                    </div>
                </div>
            </div> <!-- Row END -->
        </div>
    </section>
    <!-- =======================
    Latest news slider END -->

    <!-- =======================
    Main hero START -->
    <section class="py-0 card-grid">
        <div class="container">
            <div class="row">
                <!-- Slider START -->
                <div class="col-lg-7">
                    <div class="tiny-slider arrow-hover arrow-blur arrow-round rounded-3">
                        <div class="tiny-slider-inner"
                             data-autoplay="false"
                             data-hoverpause="true"
                             data-gutter="0"
                             data-arrow="true"
                             data-dots="false"
                             data-items="1">
                            <!-- Slide 1 -->
                            <div class="card card-overlay-bottom card-bg-scale h-400 h-lg-560 rounded-0" style="background-image:url(theme/images/blog/16by9/07.jpg); background-position: center left; background-size: cover;">
                                <!-- Card Image overlay -->
                                <div class="card-img-overlay d-flex flex-column p-3 p-sm-5">
                                    <!-- Card overlay Top -->
                                    <div class="w-100 mb-auto d-flex justify-content-end">
                                        <div class="text-end ms-auto">
                                            <!-- Card format icon -->
                                            <div class="icon-md bg-primary-soft bg-blur text-white rounded-circle" title="This post has video"><i class="fas fa-video"></i></div>
                                        </div>
                                    </div>
                                    <!-- Card overlay Bottom  -->
                                    <div class="w-100 mt-auto">
                                        <div class="col">
                                            <!-- Card category -->
                                            <a href="#" class="badge bg-primary mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Business</a>
                                            <!-- Card title -->
                                            <h2 class="text-white display-5"><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-normal">Never underestimate the influence of social media</a></h2>
                                            <p class="text-white">For who thoroughly her boy estimating conviction. Removed demands expense account in outward tedious do.</p>
                                            <!-- Card info -->
                                            <ul class="nav nav-divider text-white-force align-items-center d-none d-sm-inline-block">
                                                <li class="nav-item">
                                                    <div class="nav-link">
                                                        <div class="d-flex align-items-center text-white position-relative">
                                                            <div class="avatar avatar-sm">
                                                                <img class="avatar-img rounded-circle" src="theme/images/avatar/01.jpg" alt="avatar">
                                                            </div>
                                                            <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Carolyn</a></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="nav-item">Jan 26, 2021</li>
                                                <li class="nav-item">3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 2 -->
                            <div class="card card-overlay-bottom card-bg-scale h-400 h-lg-560 rounded-0" style="background-image:url(theme/images/blog/16by9/08.jpg); background-position: center left; background-size: cover;">
                                <!-- Card Image overlay -->
                                <div class="card-img-overlay d-flex align-items-center p-3 p-sm-5">
                                    <div class="w-100 mt-auto">
                                        <div class="col">
                                            <!-- Card category -->
                                            <a href="#" class="badge bg-danger mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Lifestyle</a>
                                            <!-- Card title -->
                                            <h2 class="text-white display-5"><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-normal">This is why this year will be the year of startups</a></h2>
                                            <p class="text-white">Particular way thoroughly unaffected projection favorable Mrs can be projecting own. </p>
                                            <!-- Card info -->
                                            <ul class="nav nav-divider text-white-force align-items-center d-none d-sm-inline-block">
                                                <li class="nav-item">
                                                    <div class="nav-link">
                                                        <div class="d-flex align-items-center text-white position-relative">
                                                            <div class="avatar avatar-sm">
                                                                <div class="avatar-img rounded-circle bg-info">
                                                                    <span class="text-white position-absolute top-50 start-50 translate-middle fw-bold small">WB</span>
                                                                </div>
                                                            </div>
                                                            <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Louis</a></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="nav-item">Nov 15, 2021</li>
                                                <li class="nav-item">5 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Slider END -->
                <div class="col-lg-5 mt-5 mt-lg-0">
                    <!-- Card item START -->
                    <div class="card mb-4">
                        <div class="row g-3">
                            <div class="col-4">
                                <img class="rounded-3" src="theme/images/blog/4by3/01.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <a href="#" class="badge bg-danger mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Lifestyle</a>
                                <h5><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-bold">The pros and cons of business agency</a></h5>
                                <!-- Card info -->
                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block small">
                                    <li class="nav-item">
                                        <div class="nav-link">
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="avatar avatar-xs">
                                                    <img class="avatar-img rounded-circle" src="theme/images/avatar/01.jpg" alt="avatar">
                                                </div>
                                                <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Samuel</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">Jan 22, 2021</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Card item END -->
                    <!-- Card item START -->
                    <div class="card mb-4">
                        <div class="row g-3">
                            <div class="col-4">
                                <img class="rounded-3" src="theme/images/blog/4by3/02.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <a href="#" class="badge bg-info mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Sports</a>
                                <h5><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-bold">5 reasons why you shouldn't startup</a></h5>
                                <!-- Card info -->
                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block small">
                                    <li class="nav-item">
                                        <div class="nav-link">
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="avatar avatar-xs">
                                                    <img class="avatar-img rounded-circle" src="theme/images/avatar/02.jpg" alt="avatar">
                                                </div>
                                                <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Dennis</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">Mar 07, 2021</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Card item END -->
                    <!-- Card item START -->
                    <div class="card mb-4">
                        <div class="row g-3">
                            <div class="col-4">
                                <img class="rounded-3" src="theme/images/blog/4by3/03.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <a href="#" class="badge bg-success mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Business</a>
                                <h5><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-bold">Five unbelievable facts about money.</a></h5>
                                <!-- Card info -->
                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block small">
                                    <li class="nav-item">
                                        <div class="nav-link">
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="avatar avatar-xs">
                                                    <img class="avatar-img rounded-circle" src="theme/images/avatar/03.jpg" alt="avatar">
                                                </div>
                                                <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Bryan</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">Jun 17, 2021</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Card item END -->
                    <!-- Card item START -->
                    <div class="card mb-4">
                        <div class="row g-3">
                            <div class="col-4">
                                <img class="rounded-3" src="theme/images/blog/4by3/04.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <a href="#" class="badge bg-warning mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Technology</a>
                                <h5><a href="post-single-4.html" class="btn-link text-reset stretched-link fw-bold">Around the web: 20 fabulous infographics about business</a></h5>
                                <!-- Card info -->
                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block small">
                                    <li class="nav-item">
                                        <div class="nav-link">
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="avatar avatar-xs">
                                                    <img class="avatar-img rounded-circle" src="theme/images/avatar/05.jpg" alt="avatar">
                                                </div>
                                                <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Jacqueline</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">Nov 11, 2021</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Card item END -->
                </div>
            </div> <!-- Row END -->
        </div>
    </section>
    <!-- =======================
    Main hero END -->

    <!-- =======================
    Feature News slider START -->
    <section class="py-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mb-4">
                        <h2 class="m-0"><i class="bi bi-hourglass-top me-2"></i>Editor's Pick</h2>
                        <p>Latest breaking news, pictures, videos, and special reports</p>
                    </div>
                    <div class="tiny-slider arrow-blur arrow-round rounded-3 overflow-hidden">
                        <div class="tiny-slider-inner"
                             data-autoplay="true"
                             data-hoverpause="true"
                             data-gutter="24"
                             data-arrow="true"
                             data-dots="false"
                             data-items-xl="4"
                             data-items-lg="3"
                             data-items-md="3"
                             data-items-sm="2"
                             data-items-xs="1">
                            <!-- Card item START -->

                                @if(count($posts ) > 0)
                                    @foreach($posts as $post)
                                    <div>
                                        <div class="card card-overlay-bottom card-img-scale">
                                    <!-- Card Image -->
                                    <img class="card-img" src="theme/images/blog/3by4/01.jpg" alt="">
                                    <!-- Card Image overlay -->
                                    <div class="card-img-overlay d-flex flex-column p-3 p-sm-4">
                                        <div>
                                            <!-- Card category -->
                                            <a href="#" class="badge bg-warning"><i class="fas fa-circle me-2 small fw-bold"></i>Technology</a>
                                        </div>
                                        <div class="w-100 mt-auto">

                                            <!-- Card title -->
                                            <h4 class="text-white"><a href="post-single-2.html" class="btn-link text-reset stretched-link">{{ $post->title }}</a></h4>
                                            <!-- Card info -->
                                            <ul class="nav nav-divider text-white-force align-items-center d-none d-sm-inline-block small">
                                                <li class="nav-item position-relative">
                                                    <div class="nav-link">by <a href="#" class="stretched-link text-reset btn-link">Bryan</a>
                                                    </div>
                                                </li>
                                                <li class="nav-item">Aug 18, 2021</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                    @endforeach
                                @else

                                @endif
                            <!-- Card item END -->
                            <!-- Card item START -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =======================
    Feature News slider END -->

    <!-- =======================
    Main content START -->
    <section class="position-relative">
        <div class="container" data-sticky-container>
            <div class="row">
                <!-- Main Post START -->
                <div class="col-lg-9">
                    <!-- Top highlights START -->
                    <div class="mb-4">
                        <h2 class="m-0"><i class="bi bi-hourglass-top me-2"></i>All Stories</h2>
                        <p>Latest breaking news, pictures, videos, and special reports</p>
                    </div>
                @if(count($posts ) > 0)
                    @foreach($posts as $post)
                    <!-- Card item START -->

                    <div class="card mb-4">
                        <div class="row">
                            <div class="col-md-5">
                                <img class="rounded-3" src="theme/images/blog/4by3/02.jpg" alt="">
                            </div>
                            <div class="col-md-7 mt-3 mt-md-0">
                                <a href="#" class="badge bg-info mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Sports</a>
                                <h3><a href="post-single-2.html" class="btn-link stretched-link text-reset">{{ $post->title }}</a></h3>
                                <p> {{ $post->excerpt }}</p>
                                <!-- Card info -->
                                <ul class="nav nav-divider align-items-center d-none d-sm-inline-block">
                                    <li class="nav-item">
                                        <div class="nav-link">
                                            <div class="d-flex align-items-center position-relative">
                                                <div class="avatar avatar-xs">
                                                    <img class="avatar-img rounded-circle" src="theme/images/avatar/02.jpg" alt="avatar">
                                                </div>
                                                <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Dennis</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">{{ $post->created_at }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Card item END -->
                    @endforeach
                @else

                @endif

                    <!-- Adv -->
                    <div>
                        <a href="#" class="card-img-flash d-block mt-4">
                            <img src="theme/images/adv-1.png" alt="adv">
                        </a>
                    </div>

                </div>
                <!-- Main Post END -->
                <!-- Sidebar START -->
                <div class="col-lg-3 mt-5 mt-lg-0">
                    <div data-sticky data-margin-top="80" data-sticky-for="767">
                        <!-- Social links -->
                        <div class="row g-2">
                            <a href="#" class="d-flex justify-content-between align-items-center bg-facebook text-white-force rounded p-2 position-relative">
                                <i class="fab fa-facebook-square fs-3"></i>
                                <div class="d-flex">
                                    <h6 class="me-1 mb-0">1.5K</h6>
                                    <small class="small">Fans</small>
                                </div>
                            </a>
                            <a href="#" class="d-flex justify-content-between align-items-center bg-instagram-gradient text-white-force rounded p-2 position-relative">
                                <i class="fab fa-instagram fs-3"></i>
                                <div class="d-flex">
                                    <h6 class="me-1 mb-0">1.8M</h6>
                                    <small class="small">Followers</small>
                                </div>
                            </a>
                            <a href="#" class="d-flex justify-content-between align-items-center bg-youtube text-white-force rounded p-2 position-relative">
                                <i class="fab fa-youtube-square fs-3"></i>
                                <div class="d-flex">
                                    <h6 class="me-1 mb-0">22K</h6>
                                    <small class="small">Subscribers</small>
                                </div>
                            </a>
                        </div>
                        <!-- Categories -->
                        <div class="row g-2 mt-5">
                            <h5>Categories</h5>
                            <div class="d-flex justify-content-between align-items-center bg-warning-soft rounded p-2 position-relative">
                                <h6 class="m-0 text-warning">Photography</h6>
                                <a href="#" class="badge bg-warning text-dark stretched-link">09</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-center bg-info-soft rounded p-2 position-relative">
                                <h6 class="m-0 text-info">Travel</h6>
                                <a href="#" class="badge bg-info stretched-link">25</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-center bg-danger-soft rounded p-2 position-relative">
                                <h6 class="m-0 text-danger">Photography</h6>
                                <a href="#" class="badge bg-danger stretched-link">75</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-center bg-primary-soft rounded p-2 position-relative">
                                <h6 class="m-0 text-primary">Covid-19</h6>
                                <a href="#" class="badge bg-primary stretched-link">19</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-center bg-success-soft rounded p-2 position-relative">
                                <h6 class="m-0 text-success">Business</h6>
                                <a href="#" class="badge bg-success stretched-link">35</a>
                            </div>
                        </div>
                        <!-- Most read -->
                        <div>
                            <h5 class="mt-5 mb-3">Most read</h5>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">01</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">Bad habits that people in the business industry need to quit</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">02</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">How to worst business fails of all time could have been prevented</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">03</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">How 10 worst business fails of all time could have been prevented</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">04</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">10 facts about business that will instantly put you in a good mood</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">05</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">How did we get here? The history of the business told through tweets</a></h6>
                            </div>
                            <div class="d-flex position-relative mb-3">
                                <span class="me-3 mt-n1 fa-fw fw-bold fs-3 opacity-5">06</span>
                                <h6><a href="#" class="stretched-link text-reset btn-link">Ten tips about startups that you can't learn from books</a></h6>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Sidebar END -->
            </div> <!-- Row end -->
        </div>
    </section>
    <!-- =======================
    Main content END -->



    <!-- =======================


{{--    <section class="heroBg" style="background: url('{{ asset('images/buildings.svg') }}') no-repeat bottom center  #0051d4  " >--}}
{{--        <div class="container">--}}
{{--            <div class="business-search-home text-left ">--}}
{{--                <div class="row justify-content-start">--}}
{{--                    <h2 class="headline">Bahrain's No.1 Local Search Engine</h2>--}}
{{--                    <p class="subline">Company reviews. Salaries. Interviews. Jobs.</p>--}}
{{--                    <div class="col-md-6 col-md-offset-3">--}}
{{--                        <form  action="{{ route('search') }}" method="GET">--}}

{{--                            <div class="input-group input-group-lg">--}}
{{--                                <input class="form-control form-control-lg" name="search" placeholder="Search for anything, anywhere in Bahrain" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">--}}
{{--                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Go</button> </span>--}}
{{--                            </div>--}}
{{--                            <!-- /input-group -->--}}
{{--                        </form>--}}
{{--                    </div>--}}



{{--                </div>--}}

{{--                <div class="row mt-3">--}}
{{--                    <div class="col-sm-12">--}}
{{--                        <a href="#">Web Design Company In Bahrain</a> |--}}
{{--                        <a href="#">Attractions</a> |--}}
{{--                        <a href="#">Stories</a> |--}}
{{--                        <a href="#">Guides</a>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- row -->--}}
{{--            </div>--}}

{{--        </div>--}}


{{--    </section>--}}
{{--    <!-- /hero_single -->--}}
{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="popular_categories">--}}
{{--                    <div class="col-lg-12">--}}
{{--                        <h3>Popular Categories</h3>--}}
{{--                        <ul>--}}
{{--                            @if(count($categoriesList ) > 0)--}}
{{--                                @foreach($categoriesList as $category)--}}
{{--                                    <li >--}}
{{--                                        <a href="{{ url('/category/'.$category->slug ) }}">{{ $category->title }} Companies in Bahrain</a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            @else--}}
{{--                                <li>--}}
{{--                                    <a class="#">Nothing Found</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}

{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <div class="col-5">--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header">--}}
{{--                            <h2 class=" text-primary card-header-title">Newsletter</h2>--}}
{{--                        </div>--}}

{{--                        <div class="card-body">--}}
{{--                            <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>--}}
{{--                            <form  action="{{ route('search') }}" method="GET">--}}

{{--                                <div class="input-group input-group-lg">--}}
{{--                                    <input class="form-control form-control-lg" name="search" placeholder="Your email address" type="text" aria-label="Search for anything, anywhere in India" aria-describedby="button-addon2">--}}
{{--                                    <span class="input-group-btn"><button class="btn btn-lg btn-primary"  type="submit">Subscribe</button> </span>--}}
{{--                                </div>--}}
{{--                                <!-- /input-group -->--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                   <div class="card">--}}
{{--                       <div class="card-header">--}}
{{--                           <h2 class=" text-primary card-header-title">Companies in Bahrain</h2>--}}
{{--                       </div>--}}

{{--                       <div class="card-body">--}}
{{--                           <p>Join 33,000+ others and never miss out on new tips, tutorials, and more.</p>--}}
{{--                           <a href="{{ url('/') }}" class="btn btn-primary btn-lg  me-2">View All Businesses</a>--}}
{{--                       </div>--}}
{{--                   </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-12">--}}
{{--                    <h1>Over  {{ \App\Models\Business::whereIn('cr_status', array('ACTIVE'))->count()  }} companies</h1>--}}
{{--                    <p>Get in depth information about companies. Know about their mission, vision, products, services and much more.</p>--}}
{{--                </div>--}}
{{--                <div id="companiesWrap">--}}
{{--                    @if(count($businesses ) > 0)--}}
{{--                        @foreach($businesses as $business)--}}
{{--                        <div class="company_item" ><a href="{{ url('/businesses/'.$business->slug ) }}" title="{{ $business->title }}"><img class="company_logo" src="https://static.ambitionbox.com/static/icons/company-placeholder.svg" onerror="this.onerror=null;this.src='https://static.ambitionbox.com/static/icons/company-placeholder.svg';" alt="{{ $business->title  }}"></a></div>--}}



{{--                        @endforeach--}}
{{--                    @else--}}
{{--                        <li>--}}
{{--                            <a class="#">Nothing Found</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}

{{--                <div class="col-lg-12">--}}
{{--                    <h2>Bahrain News & Updates</h2>--}}
{{--                </div>--}}

{{--                @if(count($posts ) > 0)--}}
{{--                    @foreach($posts as $post)--}}
{{--                        <div class="col-lg-4">--}}
{{--                            <div class="card">--}}
{{--                                <!-- Card img -->--}}
{{--                                <div class="position-relative">--}}
{{--                                    <img class="card-img" src="{{ asset('https://www.cibjo.org/wp-content/uploads/2019/04/Four-Seasons-1-lr.jpg')  }}" alt="Card image">--}}
{{--                                    <div class="card-img-overlay d-flex align-items-start flex-column p-3">--}}
{{--                                        <!-- Card overlay Top -->--}}
{{--                                        <div class="w-100 mb-auto d-flex justify-content-end">--}}
{{--                                            <div class="text-end ms-auto">--}}
{{--                                                <!-- Card format icon -->--}}
{{--                                                <div class="icon-md bg-white-soft bg-blur text-white rounded-circle" title="This post has video"><i class="fas fa-video"></i></div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Card overlay bottom -->--}}
{{--                                        <div class="w-100 mt-auto">--}}
{{--                                            <!-- Card category -->--}}
{{--                                            <a href="#" class="badge bg-danger mb-2"><i class="fas fa-circle me-2 small fw-bold"></i>Travel</a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="card-body">--}}
{{--                                    <h4 class="card-title"><a href="{{ $post->slug }}" class="btn-link text-reset fw-bold">{{ $post->title }}</a></h4>--}}
{{--                                    <p class="card-text">{{ $post->excerpt }}</p>--}}
{{--                                    <!-- Card info -->--}}
{{--                                    <ul class="nav nav-divider align-items-center d-none d-sm-inline-block">--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <div class="nav-link">--}}
{{--                                                <div class="d-flex align-items-center position-relative">--}}
{{--                                                    <div class="avatar avatar-xs">--}}
{{--                                                        <img class="avatar-img rounded-circle" src="assets/images/avatar/02.jpg" alt="avatar">--}}
{{--                                                    </div>--}}
{{--                                                    <span class="ms-3">by <a href="#" class="stretched-link text-reset btn-link">Dennis</a></span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        <li class="nav-item">{{ $post->published_at }}</li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}



{{--                    @endforeach--}}
{{--                @else--}}
{{--                    <li>--}}
{{--                        <a class="#">Nothing Found</a>--}}
{{--                    </li>--}}
{{--                @endif--}}


{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <h2>Why Gulfthis Community</h2>--}}

{{--                </div>--}}

{{--               6--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </section>--}}

@endsection
