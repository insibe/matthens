
@extends('layouts.app')
@section('custom-scripts-css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container  my-md-4 ">
    <div class="row justify-content-center my-3">
        <div class="col-lg-8 bg-white businessCard">
            <div class="row g-3">
                <div class="col-lg-7">
                    <h1>{{ $page_title }}</h1>
                </div>

                {!! Form::model($business, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'row g-4' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Company Name</label>
                            {!! Form::text('cr_en',null, ['class' => 'form-control', 'placeholder'=>'Enter Category Title..','required' =>'required']) !!}
                        </div>
                        <div class="col-md-12">
                            <label for="inputPassword4" class="form-label">Company Description (in English please)</label>
                            {!! Form::textarea('description',null, ['rows'=>'2', 'placeholder'=>'Enter Category Description..','class' => 'form-control',]) !!}
                        </div>
                        <div class="col-md-6">
                            <label for="inputPassword4" class="form-label">Email</label>
                            <input type="password" class="form-control" id="inputPassword4">
                        </div>
                        <div class="col-md-6">
                            <label for="inputPassword4" class="form-label">Website URL</label>
                            <input type="password" class="form-control" id="inputPassword4">
                        </div>

                        <hr>


                        <div class="col-12">
                            <label for="category" class="form-label">Primary Category</label>
                            {!! Form::select('parent_id', $categoriesPrimary ,null, ['class' => 'form-control form-select','data-choices'=>'on' ,'data-search'=>'on','required' =>'required']) !!}
                        </div>
                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">Email</label>
                    <input type="email" class="form-control" id="inputEmail4">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Password</label>
                    <input type="password" class="form-control" id="inputPassword4">
                </div>
                <div class="col-12">
                    <label for="inputAddress" class="form-label">Address</label>
                    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                </div>
                <div class="col-12">
                    <label for="inputAddress2" class="form-label">Address 2</label>
                    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                </div>
                <div class="col-md-6">
                    <label for="inputCity" class="form-label">City</label>
                    <input type="text" class="form-control" id="inputCity">
                </div>
                <div class="col-md-4">
                    <label for="inputState" class="form-label">State</label>
                    <select id="sss" class="form-select">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="inputState" class="form-label">Country</label>
                    <select id="s" class="form-select">
                        {!! Form::select('country',['Bahrain'], null, ['class' => 'form-control form-select', 'required' =>'required']) !!}
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="inputZip" class="form-label">Zip</label>
                    <input type="text" class="form-control" id="inputZip">
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label" for="gridCheck">
                            Check me out
                        </label>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>







                {!! Form::close() !!}
            </div>
        </div>

    </div>
</div>
@endsection


@section('custom-scripts-js')


@endsection