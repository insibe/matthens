
@extends('layouts.app')
@section('custom-scripts-css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container  my-md-4 ">
   <div class="row justify-content-center my-3">
       <div class="col-lg-8 bg-white businessCard">
           <div class="row g-3">
               <div class="col-lg-12">
                   <h1>{{ $page_title }}</h1>
               </div>
               {!! Form::model($category, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'row g-4' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
               <div class="col-sm-12">
                   <label for="CategoryTitle" class="form-label">Category Title</label>
                   {!! Form::text('title',null, ['class' => 'form-control', 'placeholder'=>'Enter Category Title..','required' =>'required']) !!}

               </div>
               <div class="col-sm-12">
                   <label for="lastName" class="form-label"> Category Description </label>
                   {!! Form::textarea('description',null, ['rows'=>'2', 'placeholder'=>'Enter Category Description..','class' => 'form-control',]) !!}


               </div>
               <div class="col-md-12">
                   <label class="form-label">Parent Category <span class="text-danger">*</span></label>
                   {!! Form::select('parent_id', $categoriesList ,null, ['class' => 'form-control form-select', 'data-search'=>'on','required' =>'required']) !!}
               </div>

               <div class="col-md-12">
                  <div class="form-group">
                      <label for="country" class="form-label">Status</label>
                      {!! Form::select('status',['Free','Featured','Sponsored'], null,['class' => 'form-control form-select','data-search'=>'on']) !!}
                  </div>

               </div>
                 <div class="col-lg-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-lg  btn-outline-gray mr-auto" >Discard</button>
                        <button type="submit" class="btn btn-lg btn-primary ">Save</button>
                    </div>
                 </div>
               {!! Form::close() !!}
           </div>
       </div>

   </div>
</div>
@endsection


@section('custom-scripts-js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
   <script type="application/javascript">
       $('.form-select').select2({
       });
   </script>
@endsection