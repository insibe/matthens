@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>Bahrain Public & National Holidays (2021)</h1>
                    <p> Consist mostly of Islamic Holidays and are based on the lunar calendar subject to moon sighting.
                        Therefore, the dates of these Islamic holidays vary from year to year.</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Weekday</th>
                                    <th>Holiday name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Jan 1</td>
                                    <td>Fri</td>
                                    <td>New Year</td>
                                </tr>
                                <tr>
                                    <td>Mar 20</td>
                                    <td>Sat</td>
                                    <td>March equinox</td>
                                </tr>
                                <tr>
                                    <td>May 1</td>
                                    <td>Sat</td>
                                    <td>May Day/Labor Day</td>
                                </tr>
                                <tr>
                                    <td>May 12</td>
                                    <td>Wed</td>
                                    <td>Eid al Fitr</td>
                                </tr>
                                <tr>
                                    <td>May 13</td>
                                    <td>Thu</td>
                                    <td>Eid al Fitr</td>
                                </tr>
                                <tr>
                                    <td>May 14</td>
                                    <td>Fri</td>
                                    <td>Eid al Fitr</td>
                                </tr>
                                <tr>
                                    <td>Jun 21</td>
                                    <td>Mon</td>
                                    <td>June Solstice</td>
                                </tr>
                                <tr>
                                    <td>Jul 18 – 19</td>
                                    <td>Sun – Mon</td>
                                    <td>Arafat Day</td>
                                </tr>
                                <tr>
                                    <td>Jul 19</td>
                                    <td>Mon</td>
                                    <td>Eid al Adha></td>
                                </tr>
                                <tr>
                                    <td>Jul 20</td>
                                    <td>Tue</td>
                                    <td>Eid al Adha</td>
                                </tr>
                                <tr>
                                    <td>Jul 21</td>
                                    <td>Wed</td>
                                    <td>Eid al Adha</td>
                                </tr>
                                <tr>
                                    <td>Jul 22</td>
                                    <td>Thu</td>
                                    <td>Eid al Adha</td>
                                </tr>
                                <tr>
                                    <td>Aug 10</td>
                                    <td>Tue</td>
                                    <td>Muharram</td>
                                </tr>
                                <tr>
                                    <td>Aug 17</td>
                                    <td>Tue</td>
                                    <td>Ashoora</td>
                                </tr>
                                <tr>
                                    <td>Aug 18</td>
                                    <td>Wed</td>
                                    <td>Ashoora (2nd day)</td>
                                </tr>
                                <tr>
                                    <td>Sep 22</td>
                                    <td>Wed</td>
                                    <td>September equinox</td>
                                </tr>
                                <tr>
                                    <td>Oct 18 – 19</td>
                                    <td>Mon – Tue</td>
                                    <td>The Prophet’s Birthday</td>
                                </tr>
                                <tr>
                                    <td>Dec 16</td>
                                    <td>Thu</td>
                                    <td>National Day</td>
                                </tr>
                                <tr>
                                    <td>Dec 17</td>
                                    <td>Fri</td>
                                    <td>National Day (second day)</td>
                                </tr>
                                <tr>
                                    <td>Dec 21</td>
                                    <td>Tue</td>
                                    <td>December Solstice</td>
                                </tr>
                                </tbody>
                            </table>


                        </div>


                    </div>


                </div>
                <div class="col-lg-3 ">

                </div>
            </div>


            <div class="row">

            </div>


        </div>
    </section>
@endsection