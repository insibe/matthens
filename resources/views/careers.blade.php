
@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff  ">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>Careers @ Gulfthis</h1>
                    <p>Have that vibrant vibe in you? We need you to work with us</p>
                </div>
            </div>
        </div>
    </section>
<section>
    <div class="container  my-md-4 ">

        <div class="row my-3">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">

                        <p class="text-left">At Gulfthis, we pride ourselves on building powerful, sophisticated software that’s fun and easy to use. We believe that customers are the foundation of a successful business and we want to enable every team to deliver moments of wow to them.</p>

                        <p class="text-left">  We are constantly on the lookout for smart people who are passionate about building great products, designing great experiences, building scalable platforms, and making customers happy. If this describes you, feel free to take a look at our openings below and apply!</p>

                        <p class="text-left">Check them out here Open Positions. We'll be happy to hear from you.
                            If you’re interested to work with us, just send across your resume to us at <a href="#">careers@gulfthis.com</a> </p>
                        <p>  <a class="m-btn m-btn-theme2nd" href="#">View Openings</a></p>
                    </div>



                </div>
            </div>


        </div>
        <div class="col-lg-3 ">


        </div>
    </div>
</section>






@endsection