@extends('layouts.app')
@section('content')
    <section class="inner-hero"
             style="background: #f6f7ff">
        <div class="container">
            <div class="business-search-home text-left ">
                <div class="row justify-content-start">
                    <h1>Editorial Guidelines</h1>
                    <p>Gulfthis, we take great pride in the quality of our content. Our writers create original,
                        accurate, engaging content that is free of ethical concerns or conflicts. If you ever come
                        across an article that you think needs to be improved, please reach out by emailing <a href="#">contact@gulfthis.com</a>
                        .
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  my-md-4 ">

            <div class="row my-3">
                <div class="col-lg-12 policy-content ">


                    <h5>Shorts News - News in 60 Words</h5>
                    <p> By acting as news aggregators, we provide information in a summarized manner within a
                        limit of 60 words.</p>

                    <p>To establish the accuracy, credibility and verify the facts we check different sources
                        and then proceed working</p>

                    <p> The people working in our team ready a single article at various platforms and
                        understanding the gist of the information, they curate a summarized version of the
                        information
                    </p>
                    <p> By validating facts and information from various sources, we ensure that the summarized
                        information is un-opinionated and un-biased</p>
                    <h5> Strong Review System</h5>

                    <p> We follow a practice of multiple-review layer. One summarized content before getting
                        published on our platform goes through a strong review system.
                    </p>
                    <p> First is the self-review process where the writer checks the content for spelling,
                        grammatical, contextual, formation and language errors
                    </p>
                    <p> The next review is done by the content editor who ensures the content guidelines are
                        followed well
                    <p>
                        The next is checking the content for plagiarism levels and grammatical assurance through
                        various software available in the market
                    </p>
                    <h5> Plagiarism: NO!</h5>
                    <p> The platform guidelines focus on avoiding plagiarism on every possible level. We believe
                        & focus on providing content as licensed by our partners on the platform
                    </p>
                    <p> The content guidelines of the platform ensure to implement a through & strict plagiarism
                        check.
                    </p>
                    <p> The editor focuses on strictly looking out for possible plagiarism in the content that
                        is going to be published on the platform
                    </p>
                    <p> We seek technological help to avoid plagiarism on the platform. Using highly-reliable
                        software available in the market, we eliminate the minimalist possibilities of
                        plagiarism.
                    </p>
                    <h5> Taking Care of Intellectual Property Rights</h5>

                    <p> Apart from taking strict & legal actions for protecting our Intellectual Property
                        Rights, we believe in respecting the Intellectual Property Rights of other platforms
                    </p>
                    <p> We ensure that content going on our platform in the form of images, videos, or audios
                        are available on an open-to-use basis or are available to use under the specific
                        guidelines and licenses.
                    </p>
                    <p> Amid the through internal checking procedure, we ensure that each of the content
                        material is subject to appropriate copyrights.
                    </p>
                    <h5> Language & Style Consistency</h5>

                    <p> The detailed editorial policy provided to the writing department comprises of a common
                        guideline for writing style, grammar, language, etc., to ensure consistency and
                        similarity in our contents.
                    </p>


                </div>



            </div>
            <div class="col-lg-3 ">


            </div>
        </div>
    </section>





@endsection