
@extends('layouts.app')

@section('title', strtolower($business->cr_en) )
@section('meta_keywords',strtolower($business->cr_en),'Bahrain')
@section('meta_description', 'Contact page of example.com')

@section('content')

<div class="container  my-md-4 ">
    <div class="row my-3 " id="follow">
        <div class="col-lg-12">
           <div class="card">
               <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
               <!-- gulfthis- horizontal -->
               <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-2126121144705193"
                    data-ad-slot="7223527825"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>
               <script>
                   (adsbygoogle = window.adsbygoogle || []).push({});
               </script>
           </div>
        </div>
    </div>
   <div class="row my-3">
       <div class="col-lg-9">
           <div class="card">
               <div class="card-header">

                   <h2 class=" card-header-title me-auto">
                       {{ ucwords(strtolower($business->cr_en))   }}
                   </h2>



                   @if (Auth::check())
                       <a href="{{ url('businesses/'.$business->id.'/edit') }}" class="btn btn-sm btn-outline-secondary">
                           Edit this Business
                       </a>
                   @else

                       <a href="#!" class="btn btn-sm btn-outline-secondary">
                           Claim this business
                       </a>
                   @endif

               </div>
               <div class="card-body">
                   <p>{{ $business->description }}</p>
                   <p>{{ ucwords(strtolower($business->cr_en))}} is a registered as a {{ $business->cr_type }}  on {{ Carbon\Carbon::parse($business->date_registered)->format('l jS F Y') }} in the Kingdom of Bahrain. {{ ucwords(strtolower($business->cr_en))}}'s CR Number is {{ $business->cr_no }}.{{ ucwords(strtolower($business->cr_en))}} is in the industry of:
                       <a href="{{ url('category/'.$business->categories->first()->slug) }}"> {{ $business->categories->first()->title }}</a> and the company is based on Bahrain.</p>

                   <div class="popular_categories col">
                       <ul>
                           @foreach($business->categories as $category )
                               <li>
                                   <h4><a href="{{ url('category/'.$category->slug) }}">{{ $category->title }}</a></h4>
                               </li>
                           @endforeach

                       </ul>
                   </div>

               </div>
               <div class="card-footer">
                  <p class="small"> Last updated on {{ Carbon\Carbon::parse($business->updated_at)->format('jS F Y') }}</p>
               </div>
           </div>
           <div class="card">
               <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
               <!-- gulfthis- horizontal -->
               <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-2126121144705193"
                    data-ad-slot="7223527825"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>
               <script>
                   (adsbygoogle = window.adsbygoogle || []).push({});
               </script>
           </div>
           <div class="card">
               <div class="card-header">
                   <h4 class="d-block">Company Information</h4>
               </div>
               <div class="card-body">
                   <p> Find other contact information for {{ ucwords(strtolower($business->cr_en))}} such as Email, Website and more below.</p>
                   <table class="table table-striped">
                       <thead>

                       </thead>
                       <tbody>
                       <tr>
                           <td scope="row">CR Number</td>
                           <td scope="row">{{ $business->cr_no }}</td>
                       </tr>


                       <tr>
                           <td scope="row">Company Type</td>
                           <td scope="row">{{ $business->cr_type }}</td>
                       </tr>
                       <tr>
                           <td scope="row">Country of Incorporation</td>
                           <td scope="row">{{ $business->country }}</td>
                       </tr>
                       <tr>
                           <td scope="row">Date of Establishment</td>
                           <td colspan="2">  {{ Carbon\Carbon::parse($business->date_registered)->format('j F Y') }} </td>

                       </tr>

                       <tr>
                           <td scope="row">Age of Company</td>
                           <td colspan="2">  {{ \Carbon\Carbon::parse($business->date_registered)->diff(\Carbon\Carbon::now())->format('%y years, %m months and %d days')}}</td>


                       </tr>
                       </tbody>
                   </table>
               </div>
           </div>
           <div class="card">
               <div class="card-header">
                   <h4 class="d-block">Latest Jobs opening in {{ ucwords(strtolower($business->cr_en))}} </h4>
               </div>
               <div class="card-body">
                   <p>Currently, there are no vacancies in {{ ucwords(strtolower($business->cr_en))}}</p>
               </div>
           </div>
           <div class="card">
               <div class="card-header">
                   <h3 class="card-header-title me-auto">
                       Frequently Asked Questions regarding {{ ucwords(strtolower($business->cr_en)) }} ?
                   </h3>
               </div>

               <div class="card-body">
                   <h4>Where are {{ ucwords(strtolower($business->cr_en))}}   ’s Company Located?</h4>
                   <p>{{ ucwords(strtolower($business->cr_en))}} company located in Kingdom of Bahrain</p>

                   <h4>When was the {{ ucwords(strtolower($business->cr_en)) }} incorporated?</h4>
                   <p>The {{ ucwords(strtolower($business->cr_en)) }} was incorporated with MOIC on  {{ Carbon\Carbon::parse($business->date_registered)->format('j F Y') }} as {{ $business->cr_type }}.</p>
                   <h4>What is {{ ucwords(strtolower($business->cr_en))}}’s CR Number?</h4>
                   <p> {{ ucwords(strtolower($business->cr_en))}}’s CR Number: {{ $business->cr_no }}</p>
                   <h4> What is {{ ucwords(strtolower($business->cr_en))}}s’s industry?</h4>
                   <p> {{ ucwords(strtolower($business->cr_en))}} is in the industry of: {{ $business->categories->first()->title }}</p>
               </div>
           </div>








       </div>
       <div class="col-lg-3 ">

           <div class="card">
               <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
               <!-- gulfthis Square -->
               <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-2126121144705193"
                    data-ad-slot="2678859726"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>
               <script>
                   (adsbygoogle = window.adsbygoogle || []).push({});
               </script>
           </div>

               <div class="card ">
                 <div class="card-body">
                     <h4> Everything you want to know about  {{ ucwords(strtolower($business->cr_en))}} ? </h4>
                     <p>Join the 10,000+ businesses whose sales and marketing teams rely on Gulfthis's B2B database to identify, connect, and close their next customer.</p>
                     <p>With plans starting at just $29/month, sign up now to unlock access to:</p>
                 </div>
               </div>


           <div class="card ">
                   <div class="card-body">
                       <h4>Gulfthis App</h4>
                       <p> A smarter way to search for the local business in Bahrain.</p>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zeroPadding"> <img class=" img-responsive " data-qk-el-name="imglazyload" src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" data-original-src="https://assets.quickerala.com/ui/build/images/app-bg-right.png" alt="Gulfthis mobile app" title="gulfthis mobile app"> </div>
                   </div>
           </div>
       </div>
   </div>





</div>
@endsection